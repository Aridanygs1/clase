

var createError = require('http-errors');             // IMPORT
var express = require('express');		      // IMPORT
var path = require('path');                           // IMPORT
var cookieParser = require('cookie-parser');          // IMPORT
var logger = require('morgan');                       // IMPORT

var indexRouter = require('./routes/index');          // IMPORT
var usersRouter = require('./routes/users');          // IMPORT

var app = express();				      // INSTANCIA



// ===== HE TOCADO AQUÍ =========

	var favicon = require( "serve-favicon");     // IMPORT
	app.use( favicon( path.join( __dirname , "public" , "favicon.ico" ) ) );


// ==============================



// view engine setup
app.set('views', path.join(__dirname, 'views'));   // CONFIGURACIONES
app.set('view engine', 'pug');			   // CONFIGURACIONES

app.use(logger('dev'));				   // MIDLEWARE
app.use(express.json());			   // MIDLEWARE
app.use(express.urlencoded({ extended: false }));  // MIDLEWARE 
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);			// ROUTER
app.use('/users', usersRouter);			// ROUTER

app.use(function(req, res, next) 		// HANDLER
{
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) 		// HANDLER
{
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;				// SERVER EXPORT



