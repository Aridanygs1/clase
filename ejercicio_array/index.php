<!DOCTYPE html>
<html lang="es">
<head>
	<title>WebSite: Array</title>
	<style>
			table
			{

				margin-top:10px;
				border-collapse:collapse;
			}

			table td 
			{
				padding:5px;
				border:1px solid black;
			}

	</style>
</head>

<?php

	require_once( "func.php" );


	$alumnos = array( );
	$nombres = array( "Luis" , "Agustin" , "Aridany" );


	echo "<h2>Ejercicio 2</h2>";
	echo "[2] El array contiene: ".(count($nombres))." elementos.<br/>";
	
	echo "<h2>Ejercicio 3</h2>";
	echo "[3] Nombres: ".implode( " " , $nombres ).".<br/>";


	echo "<h2>Ejercicio 4</h2>";
	asort( $nombres );
	echo "[4] Ordenado:".implode( " " , $nombres).".<br/>";


	echo "<h2>Ejercicio 5</h2>";
	echo "<br/>KSORT:<br/>";
	ksort( $nombres );
	print_r( $nombres );


	echo "<br/><br/>";
	echo "SORT:<br/>";
	sort( $nombres );
	print_r( $nombres );

	echo "<h2>Ejercicio 6</h2>";
	echo "<br/><br/>[6] Orden inverso:  ";
		print_r( array_reverse($nombres) );
		
	echo ".<br/>";

	echo "<h2>Ejercicio 7 </h2>";
	echo "[7] Posición de \"Aridany\": ".array_search( "Aridany" , $nombres ).".<br/>";


	echo "<h2>Ejercicio 8</h2>";
	addAlumn( "alumnos" , "Aridany"  , 24 );
	addAlumn( "alumnos" , "Luis"     , 20 );
	addAlumn( "alumnos" , "Agustin"  , 19 );

	print_r( $alumnos );

	echo "<h2>Ejercicio 9</h2>";
	printTable( $alumnos );

	echo "<h2>Ejercicio 10</h2>";
	echo "[10] array_column: ";
		print_r( array_column( $alumnos , "alumno" ) );
	
	echo ".<br/>";


	echo "<h2>Ejercicio 11</h2>";
	$numeros = array();
	createArraySize( "numeros" );

	echo "Resultado:".array_sum( $numeros );	
?>
</body>
</html>
