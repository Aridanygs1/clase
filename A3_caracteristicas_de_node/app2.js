
	const express = require( "express" );
	const app     = express();
	const PORT    = 3000;
	const fecha   = require("./fecha.js");

	const logger  = require( "morgan" );


	// Ejercicio 2
	app.get( "/" , (  req , res ) => 
	{
		res.status(200).send( fecha.getFecha() );
	});


	// Ejercicio 3
	app.get( "/about" , ( req , res ) => 
	{
		res.status(200).send( "Aridany 24 Pizzzzzzza" );
	});


	// Ejercicio 4
	app.use( "/about2" , ( req , res , next ) => 
	{
		res.status(200).send("Aridany 24 Pizzzzzzza");
	});

	app.listen( PORT , function()
	{
			console.log("");
	});
