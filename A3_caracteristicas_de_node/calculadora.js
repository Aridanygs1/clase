

	exports.sumar = ( nums ) =>
	{
		let total = 0;
		nums.forEach(  (val) => total += val );
		return total;
	}

	exports.restar = ( nums ) => 
	{
		let total = 0;
		nums.forEach( (val) => total -= val );
		return total;
	}


	exports.dividir = ( num1 , num2 ) => (num1/num2);
	
	exports.multiplicar = ( num1 , num2 ) => ( num1 * num2 );
	
	exports.modulo = ( num1 , num2 ) => ( num1%num2) ;

	
