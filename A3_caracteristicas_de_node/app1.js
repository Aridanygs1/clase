
	// Ejercicio 1

	const PORT    = 3000;
	const http    = require( "http");
	
	var express = require( "express" ); 	
	var app     = express();

	const mates = require( "./calculadora.js" );
	const fecha = require( "./fecha.js" );
	
	console.log("\n Ejercicios:");
	console.log( "  -Suma:   10 + 20 + 30 = "         + mates.sumar ( [ 10 , 20 , 30] ) );
	console.log( "  -Resta:  39 + 40 + 12 = "         + mates.restar( [ 23 , 49 , 10 ] ) );
	console.log( "  -Módulo: 20%8 = "                 + mates.modulo( 20 , 8 ) );
	console.log( "  -División: 20/10 = "              + mates.dividir( 20 , 10 ) );
	console.log( "  -Multiplicación: 8 * 10 = "       + mates.multiplicar( 8 , 10 ) );


	console.log( "  -Fecha: " + fecha.getFecha() );


	console.log("\n");


	http.createServer( ( req , res ) => 
	{
		let text = `
			<!DOCTYPE html>
			<html lang="es">
			<head>
				<meta charset="UTF-8" />
				<title>WebSite</title>
			</head>
			<body>		
				La fecha de hoy es: ${fecha.getFecha()}
			</body>
			</html>
		`;
			
		res.writeHead( 200 , 
		{
			"Content-Type"  : "text/html",
			"Content-Length": text.length 
		});

		res.end( text );


	}).listen( PORT );



	app.get( "/" , function( req , res )
	{
			
	});

	app.listen( 3050 , function() {} );


