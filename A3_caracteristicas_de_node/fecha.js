

	exports.getFecha =  function ( ) 
	{
		let fecha = new Date();
		return (fecha.getHours()+":"+
		        fecha.getMinutes()+
		        " "+fecha.getDay()+
		        "/"+fecha.getMonth()+
		        "/"+fecha.getFullYear()
		       );
	}
