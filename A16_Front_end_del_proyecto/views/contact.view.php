<?php
	require_once_absolute ( "views/partials/inicio-doc.part.php" );
	require_once_absolute ( "views/partials/header-doc.part.php" );
?>

   <div id="contact">
   	  <div class="container">
   	    <div class="col-xs-12 col-sm-8 col-sm-push-2">
       	   <h1>CONTACT US</h1>
       	   <hr>
       	   <p>Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p>

		<?php 
			$errorManager = new FormValidate( FormValidate::FORM_CONTACT );

			if( $errorManager->isError( )  && isset($_POST["submit"] ) )  
			{
				echo "<div style='background-color:white;border:4px solid red; padding:5px;'>";
				$errorManager->printError( ) ;
				echo "</div>";
			}
		?> 	


	       <form method="POST" class="form-horizontal">
	       	  <div class="form-group">
	       	  	<div class="col-xs-6">
	       	  	    <label class="label-control">First Name</label>
				<input  name="nombre" class="form-control" value="<?= $_POST["nombre"] ?? "" ;?>" type="text">
	       	  	</div>
	       	  	<div class="col-xs-6">
	       	  	    <label class="label-control">Last Name</label>
	       	  		<input name="apellidos" class="form-control" value="<?= $_POST["apellidos"] ?? "" ;?>" type="text">
	       	  	</div>
	       	  </div>
	       	  <div class="form-group">
	       	  	<div class="col-xs-12">
	       	  		<label class="label-control">Email</label>
	       	  		<input name="email" class="form-control" value="<?= $_POST["email"] ?? "" ;?>" type="text">
	       	  	</div>
	       	  </div>
	       	  <div class="form-group">
	       	  	<div class="col-xs-12">
	       	  		<label class="label-control">Subject</label>
	       	  		<input name="subject" class="form-control" value="<?= $_POST["subject"] ?? "" ;?>" type="text">
	       	  	</div>
	       	  </div>
	       	  <div class="form-group">
	       	  	<div class="col-xs-12">
	       	  		<label class="label-control">Message</label>
	       	  		<textarea name="text" class="form-control"><?= $_POST["text"] ?? "" ;?></textarea>
	       	  		<button name="submit" class="pull-right btn btn-lg sr-button">SEND</button>
	       	  	</div>
	       	  </div>
	       </form>


		<?php
			if( !$errorManager->isError( FormValidate::ONLY_ERRORS)):  ?> 
			
				<textarea style="width:100%;background-color:white;height:400px;font-size:19px;color:black;">
					<?php echo "CONTENIDO FORMULARIO:\n\n"; print_r( $_POST ); ?> 
				</textarea>	
			
			
	       	        <?php endif; ?>


	       <hr class="divider">
	       <div class="address">
	           <h3>GET IN TOUCH</h3>
	           <hr>
	           <p>Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero.</p>
		       <div class="ending text-center">
			        <ul class="list-inline social-buttons">
			            <li><a href="#"><i class="fa fa-facebook sr-icons"></i></a>
			            </li>
			            <li><a href="#"><i class="fa fa-twitter sr-icons"></i></a>
			            </li>
			            <li><a href="#"><i class="fa fa-google-plus sr-icons"></i></a>
			            </li>
			        </ul>
				    <ul class="list-inline contact">
				       <li class="footer-number"><i class="fa fa-phone sr-icons"></i>  (00228)92229954 </li>
				       <li><i class="fa fa-envelope sr-icons"></i>  kouvenceslas93@gmail.com</li>
				    </ul>
				    <p>Photography Fanatic Template &copy; 2017</p>
		       </div>
	       </div>
	    </div>   
   	  </div>
   </div>
<!-- Principal Content Start -->

<?php
	require_once_absolute ( "views/partials/fin-doc.part.php" );
?>
