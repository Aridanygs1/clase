
<?php

	class MenuManager
	{
		public const ICON_HOME  = "fa fa-home sr-icons";
		public const ICON_BOOK  = "fa fa-bookmark sr-icons";
		public const ICON_FILE  = "fa fa-file-text sr-icons";
		public const ICON_PHONE = "fa fa-phone-square sr-icons";
		
		public const MENUS = [
					[ "nombre" => "Home" ,   "icon"  =>  self::ICON_HOME  , "page" => "index.php"   ],
					[ "nombre" => "About" ,  "icon"  =>  self::ICON_BOOK  , "page" => "about.php"   ],
					[ "nombre" => "Blog" ,   "icon"  =>  self::ICON_FILE  , "page" => "blog.php"    ],
					[ "nombre" => "Contact" , "icon" =>  self::ICON_PHONE , "page" => "contact.php" ],
		];


		public static function printMenu () 
		{
			foreach( self::MENUS as $m ) 
			{
				$e = (  basename($_SERVER["PHP_SELF"]) == $m["page"] ) ? "active" : "lien";
				
				$name = $m["nombre"];
				$page = $m["page"];
				$icon = $m["icon"];
						
	   	 		echo "<li class='$e'><a href='$page'><i class='$icon'></i> $name</a></li>";
	
			}
		}	


	}


?>
