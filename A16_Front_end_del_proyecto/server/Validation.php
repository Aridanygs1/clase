<?php


/****************************************************************************************
 *
 * Librería Validación Formularios.			Aridany <aridanyfps@gmail.com>
 *
 * 
 * 	Array asociativo para el constructor:  (* => obligatorio ) 
 *
 *					*"method"   => "get" , 
 *					*"name"     => "name" ,
 *					 "alias"    => "nombre",
 *					 "required" => true ,
 *					 "type"     => "email",
 *					 "min_size" => 5,
 *					 "max_size" => 25,
 *				
 ****************************************************************************************
 */


	class Validation
	{
		public const TYPE_DEFAULT       = 0x00;
		public const TYPE_ERROR         = 0x01;
		public const TYPE_WARNING       = 0x02;
		public const TYPE_OK		= 0x03;

		public const COLOR_DEFAULT      = "default";
		public const COLOR_ERROR        = "red";
		public const COLOR_WARNING      = "orange";
		public const COLOR_OK           = "green";

		public const TEXT_NO_TYPE       = "El campo %=name debe de ser un %=easyType.";
		public const TEXT_NO_EXIST      = "No existe el campo %=name.";
		public const TEXT_NO_MIN_SIZE   = "El campo %=alias debe de tener una longitud mínima de %=min_size.";
		public const TEXT_NO_MAX_SIZE   = "El campo %=alias debe de tener una longitud máxima de %=max_size.";

		private $datas;
		private $errorList = array();

		public function __construct ( $data = [] ) 
		{		
			$this->datas = $this->arreglar( $data ) ;
		}

		public static function getTypeCodeToColor ( $code ) 
		{
			switch( $code ) 
			{
				case self::TYPE_ERROR   : return self::COLOR_ERROR;
				case self::TYPE_WARNING : return self::COLOR_WARNING;
				case self::TYPE_OK      : return self::COLOR_OK;
			}

			return self::COLOR_DEFAULT;
		}

		private function arreglar( $lista ) 
		{
			$p = [];	
			foreach ( $lista as $l ) 
			{
				if( ! isset( $l["method"] )  )   trigger_error( "No existe una key 'method' ", E_USER_ERROR);
			        else if( !isset( $l["name"] ) )	 trigger_error( "No existe una key 'name' ", E_USER_ERROR);

				array_push( $p , 
				[
						"method"   => $l["method"],
						"name"     => $l["name"],
						"alias"    => $l["alias"]    ?? $l["name"],
						"required" => $l["required"] ?? false ,
						"type"     => $l["type"]     ?? "string",
						"min_size" => $l["min_size"] ?? -1,
						"max_size" => $l["max_size"] ?? -1,
						"text"     => 
						[
							"NO_TYPE"     => $l["text"]["NO_TYPE"]     ?? self::TEXT_NO_TYPE,  
							"NO_EXIST"    => $l["text"]["NO_EXIST"]    ?? self::TEXT_NO_EXIST, 
							"NO_MIN_SIZE" => $l["text"]["NO_MIN_SIZE"] ?? self::TEXT_NO_MIN_SIZE,
							"NO_MAX_SIZE" => $l["text"]["NO_MAX_SIZE"] ?? self::TEXT_NO_MAX_SIZE
						]
				]);
			}


			return $p;	
		}	


		private static function getArrayValue( $type ) 
		{
			return ( $type == "post" ) ? $_POST : $_GET;
		}

		private static function isValidType ( $type , $value ) 
		{
			switch( $type ) 
			{
				case "string": return is_string( $value );
				case "number": return is_int ( $value );
				case "email":  return filter_var($value, FILTER_VALIDATE_EMAIL);	
			}
			
			return true;
		}

		private function replaceVar ( $value , $peticion ) 
		{
			//str_replace( [ "%=alias" ] , [  $peticion["alias"] ] , $value );

			for( $i = 0; $i < 2; $i++)
			$value = strtr( $value , 
			[
				"%=alias"    => $peticion[ "alias" ],
				"%=type"     => $peticion[ "type" ],
				"%=min_size" => $peticion[ "min_size" ] ,
				"%=max_size" => $peticion[ "max_size" ],
				"%=name"     => $peticion[ "name" ],
				"%=easyType"  => strtr( $peticion["type"]  , [
						"string" => "texto" , "number" => "número" , "email" => "correo electrónico" ])
			] );

			return $value;
		}

		public function addText ( $text , $type = 0x00 , $actu = null ) 
		{
			array_push( $this->errorList ,
			[
				"text" => ( $actu != null ) ? $this->replaceVar( $text , $actu  ) : $text ,
				"type" => $type
			]);

		}

		public function validate ( $action = "default" ) 
		{
			foreach( $this->datas as $actu ) 
			{
				$actu_type_error =  ($actu["required"]) ? self::TYPE_ERROR : self::TYPE_WARNING;	

				if( !isset( self::getArrayValue( $actu["method"] ) [$actu["name"] ] ) ) 
					$this->addText( $actu["text"]["NO_EXIST"] , $actu_type_error , $actu  );
				else
				{
					$value           = self::getArrayValue( $actu["method"] ) [$actu["name"] ]; 

					if( !(strlen( $value ) == 0 && !$actu["required"]) || $actu["required"]  ) 
					{	
						if( !self::isValidType( $actu["type"] , $value ) )
						 $this->addText( $actu["text"] ["NO_TYPE"] , $actu_type_error , $actu  );	
						else
						{
							$value_size = ( $actu["type"] == "number" ) ? inval( $value ) : strlen( $value );
								
							if( $value_size < $actu["min_size"]  && $actu["min_size"] > -1  ) 
								$this->addText( $actu["text"]["NO_MIN_SIZE"], $actu_type_error , $actu );
							else if( $value_size > $actu["max_size"] && $actu["max_size"] > - 1)
								$this->addText( $actu["text"]["NO_MAX_SIZE"] ,$actu_type_error , $actu );	
						}
					}
				}
				
			}

			if( $action == "print" ) print_r( $this->errorList );

			return $this->errorList;
		}


	};





?>
