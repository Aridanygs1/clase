<?php

		class FormValidate
		{
			public const ERRORS_AND_OTHERS = 0x00;
		        public const ONLY_ERRORS       = 0x01;	

			public const FORM_CONTACT =
			[
					[
						"method"   => "post" , 
						"name"     => "nombre" ,
						"required" => true ,
						"min_size" => 5,
						"max_size" => 25,
					],
					[
						"method"   => "post" , 
						"name"     => "apellidos" ,
						"min_size" => 5,
						"max_size" => 25,
					],
					[
						"method"   => "post" , 
						"name"     => "subject" ,
						"required" => true ,
						"min_size" => 5,
						"max_size" => 25,
					],

					[
						"method"   => "post" , 
						"name"     => "email" ,
						"alias"    => "correo",
						"required" => true ,
						"type"     => "email",
						"min_size" => 5,
						"max_size" => 25,
					],
					[
						"method"   => "post" , 
						"name"     => "text" ,
						"alias"    => "Mensaje",
						"min_size" => 15
					]
 			];

			private $errorList = [];

			public function __construct( $data )
			{
				$this->errorList  =( new Validation( $data ) )->validate();
			}


			public function isError( $type = 0 ) 
			{
				for( $i = 0; $i < count($this->errorList); $i++ ) 
				{
					if( $this->errorList[$i]["type"] == Validation::TYPE_ERROR  || $type == self::ERRORS_AND_OTHERS ) 
					return true;
				}

				return false;
			}

			public function printError( ) 
			{
				foreach( $this->errorList as $l ) 
				{
					echo "<div style=\"color:".Validation::getTypeCodeToColor($l["type"])."\">".$l["text"]."</div>";
				}
			
			}

		};


?>
