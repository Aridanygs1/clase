
			class entidad
			{
				constructor( nombre , debilidades ) 
				{
					this.nombre = nombre;
					this.debil  = debilidades;
				}

				pierde( p , text = true) 
				{
					if( p == this.nombre ) 
					return this.nombre + " empata "+p;

					for( var i = 0; i < this.debil.length; i++)
					{
						if( p == this.debil[i])
						{
							if( text )
							{
						return this.nombre + " pierde contra "+p;

							}

							return false;
						}
					}	


					if( text )
					{
						return this.nombre + " gana contra "+p;
					}

					return false;
				}
			};

			class game
			{
				static SYMBOLS = 
				[
					new entidad( "tijera" , ["piedra","spook"] ),
					new entidad( "papel"  , ["tijera","lagarto"] ),
					new entidad( "piedra" , ["papel","spook"] ),
					new entidad( "spook"  , ["lagarto","papel"]),	
					new entidad( "lagarto" , ["tijera","piedra"]),
				];


				static getEntidad( symbol ) 
				{

					for( var i = 0; i < game.SYMBOLS.length; i++)
					{
						if( game.SYMBOLS[i].nombre == symbol )
						return game.SYMBOLS[i];
					}

					return NULL;
				}

				static getRandomSymbol( ) 
				{
					return game.SYMBOLS[ 
				Math.floor((Math.random()*game.SYMBOLS.length))
					];
				}


				static removeLog( ) 
				{
					document.querySelector("#log").innerHTML = "";
				}

				static test( removeLog = true )
				{
					let symbolPlayer;
					let symbolIA;

					if( removeLog ) 
					game.removeLog();

					game.appendLog( "<b>Testear:</b>" );

					for( var i = 0; i < game.SYMBOLS.length; i++)
					{
						symbolPlayer = game.SYMBOLS[i];
						for( var u = 0; u < game.SYMBOLS.length; u++)
						{
							symbolIA = game.SYMBOLS[u];

							game.appendLog( symbolPlayer.pierde(
								symbolIA.nombre,
								true
							));
						}
						game.appendLog("");
					}
				}

				static appendLog( text ) 
				{
					document.querySelector("#log").innerHTML += text +"<br/>";
				}

				static createPartida( value )
				{	
					let symbolPlayer = game.getEntidad( value );
					let symbolIA     = game.getRandomSymbol( ).nombre;
					
					game.appendLog( 
							symbolPlayer.pierde( symbolIA , true )
					);
				}
			};




