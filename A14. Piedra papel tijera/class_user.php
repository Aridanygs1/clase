<?php
	session_start();

	require_once( "class_text.php" );

	function createMessage( $status , $text )
	{
		echo json_encode( array(
			"status" => $status ,
			"text"   => $text
		));
	}


	function cifrar( $clave )
	{
		$salt = "XyZzy12*_";
		$f    = hash( "md5" , $salt."".$clave );

		return $f;
	}

	class user
	{
		const PAGE_USER  =  "pages/user.php";
		const PAGE_LOGIN =  "pages/login.php";
		
		private static $users = array();

		public static function addUser( $nombre , $clave )
		{



			array_push( self::$users , array(
		       		"nombre" => $nombre,
				"clave"  => cifrar( $clave )
			));
	
		}

		public static function login( $data )
		{
			foreach( self::$users as $p ) 
			{
				if($p["nombre"] == $data["nombre"] && 
				   cifrar( $data["clave"]) )
				{
				$_SESSION["usuario"] = array("nombre"=>"pedro" );
					createMessage( true , text::TEXT_OK_LOGIN );
					exit;
				}
			}

			createMessage( false , text::TEXT_ERROR_LOGIN );
		}


		public static function disconnect( ) 
		{
			if( isset( $_SESSION["usuario"] ) )
			{
				session_destroy();
			}
		}

		public static function getPage ( )
		{
			require_once( 
				(isset($_SESSION["usuario"])) ? self::PAGE_USER : self::PAGE_LOGIN 
			);
		}




	}



	user::addUser( "pedro" , "123" );



	// PETICIONES HTTP

	if( count($_GET) > 0 )
	{
		switch( key($_GET) ) 
		{
			case "disconnect":
					user::disconnect();
			break;
			case "login":
					if( !isset($_POST["user"]) || !isset($_POST["password"]))
					{
						createMessage( false ,
						"Faltan parámetros.");
						
						exit;
					}


					user::login( array(
						"nombre"=>$_POST["user"],
						"clave" =>$_POST["clave"]
					));
			break;
		}
	}


?>
