<?php
	require_once( "server/Conexion.php" );
	require_once( "server/User.php");


	if( User::isConnected( )  ) 
		require_once( "controls/control_index.php" );

	else
		SQL::requestLogin( ) ;

	if( !User::isConnected() ) 
		require_once( "controls/control_login.php" );
?>
