<?php

		require_once( "server/Conexion.php"); // Incluir la libreria de conexión + session.
		require_once( "models/model_index.php");	    // Incluir modelo DATABASE.
		require_once( "server/ProyectManager.php");
		require_once( "gui/GuiProyect.php");
		require_once( "server/User.php" );
		require_once( "server/AlertManager.php" );
		require_once( "server/Util.php" );

		$proyectFileManager = new guiProyect( new proyectManager
		( 
				config::getPathProyect( 0 )  
		));



		require_once( "views/view_index.php");		// Incluir la vista.
?>
