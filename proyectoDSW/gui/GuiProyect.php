<?php

	class guiProyect
	{
		private static $icon_proyect  = "icon_proyect.png";
		private static $icon_folder   = "icon_folder.png";
		private static $icon_file     = "icon_file.png";
		private static $icon_arrow    = "arrow.webp";

		private $manager;

		public function __construct( $manager ) 
		{
			$this->manager  = $manager;
		}

		public function print( ) 
		{
			$this->crear( $this->manager->getContent() );
		}

		public static function getHtmlImage( $actual , $nivel = 0 ) 
		{
			$type = ( $nivel ==  0 ) ? "proyect" : $actual["type"];
			$icon = self::$icon_file;


			switch( $type  ) 
			{
				case "folder":  $icon = self::$icon_folder;
				break;
				case "proyect": $icon = self::$icon_proyect;
				break;
			}

		
			
			return "<img src=\"imagenes/$icon\" class=\"icon\" />";
		}


		private function crear( $com , $nivel = 0 ) 
		{
			for( $i = 0; $i < count($com) ; $i++)
			{	
				$data_infos = array();

				$actual       = $com[$i];
				$actual_type  = $actual["type"]; 
				$margin_left  = ($nivel*20);
				$subtype      = ( $nivel == 0 ) ? "data-subtype = \"proyect\" " : "";
				$id_container = str_replace( " " , "_" , $actual["name"] )."_level_".$nivel; 
				$datas = "";
				$data_info_folder = "
								data-count_files = \"0\"
				";

				$data_infos = array(
							[ "count_files" , 0             , "folder"],
							[ "type"	, $actual_type  , "all" ],
							[ "level"	, $nivel        , "all" ],
							[ "id_container", $id_container , "all" ],
							[ "path"	, $actual["absolute"] , "all"]
				);

				for( $u = 0; $u < count($data_infos) ; $u++) 
				{
					$actu  = $data_infos[$u];

					if($actual_type == $actu[2] || $actu[2] == "all" )
					$datas .= "data-$actu[0]=\"$actu[1]\" ";
				}
				


				echo "<div style=\"font-size:14px;margin-left:".$margin_left."px;\">";
				echo "<div 
					$subtype
					onclick=\"editor.manager(this); \" 
					oncontextmenu = \" editor.contextMenu( this ); \"
					$datas
					$data_info_folder
					class=\"boxFile\"
					
				>";
						if( $actual_type == "folder" )
						echo "<img src=\"imagenes/".self::$icon_arrow."\" class=\"arrowIcon\" />";
					        else
							echo "<div style=\"width:22px;display:inline-block;height:1px;\"></div>";

						echo self::getHtmlImage( $actual , $nivel );
						echo "  ".$actual["name"];
					echo "</div>";

					echo "<div 
						style=\"display:none;\"
						id=\"$id_container\"			
					>";

						if( $actual["type"] == "folder")
						{	
							$this->crear
							( 
								proyectManager::getStruct
								( 
									$actual["absolute"] 
								) , 
								($nivel+1)
							);
						}

					echo "</div>";
				echo "</div>";

			}
			
		}

	}

?>
