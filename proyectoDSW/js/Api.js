


	class Api
	{
		// Añade un hijo al componente. Luego si el scroll estaba en su máximo
		// eje "Y" lo vuelve a bajar.
		static appendChildScrollMax( component , hijo  ) 
		{
			let isMaxScroll = (component.scrollTopMax == component.scrollTop);

			component.append( hijo );

			if( isMaxScroll ) 
			{
				component.scrollTop = component.scrollTopMax;
			}


		}

		static getDigit( num , longitud ) 
		{
			let result = (num).toString();

			for( ; result.length < longitud ; (result = "0"+result ) );

			return result;

		}

		static log( text , type = "default" ) 
		{
			let color = "white";
			let fecha = new Date();
			

			switch( type ) 
			{
				case "error": color = "red";   break;
				case "ok"   : color = "#19ff19"; break;
			}

			let com;

			if( com = document.querySelector("#console_container") ) 
			{
				let c = document.createElement("div");
				    c.className = "console_log";
				    c.style.color = color;
				    c.innerHTML = `[${ Api.getDigit( fecha.getHours() ,2) }:${ Api.getDigit( fecha.getMinutes(), 2) }] ${text}`;

				Api.appendChildScrollMax( com , c );
				 


			}


			
		}

	}
