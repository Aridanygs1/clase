				class sintaxisEditor 
				{
					static decode( text ) 
					{
						let e = ( value , color ) => `<span style="color:${color};">${value}</span>`;

						return text;
					}
				}



				class editor 
				{


					static deleteFile( element ) 
					{
						net.ajax
						({
							url : `server/requests.php?delete_file&type=${element.dataset.type}&path=${element.dataset.path}`,
							response : ( val ) => 
							{
								let a = JSON.parse( val ) ;

								if( a.status ) 
								{
									editor.removeComponent( element );		
								}
											
							}

						});
					}

					static removeComponent( element ) 
					{
						let contenedor;

							
						if( element.dataset.type == "folder" )
						{
							if( ( contenedor = document.querySelector(`#${element.dataset.id_container}` ) ) ) 
							{
								contenedor.parentElement.removeChild( contenedor );
							}
						}

						element.parentElement.removeChild( element );

					}

					static infoFile ( ) 
					{

					}

					static createComponent( element , type = "file" ) 
					{
						


						if( ! ( type == "file" || type == "folder" ) ) return false;

						element.dataset.count_files = parseInt(element.dataset.count_files)+1;
	
	

						const text  	        = prompt( ( type == "folder" ) ? "Nombre de la carpeta" : "Nombre del archivo" );
						const level_dom         = parseInt(element.dataset.level)+1;
						const id_container      = (text+"_level_"+level_dom).replaceAll( " " , "_"); 
					        const value_type_margin = ( type == "folder" ) ? 0 : 22;	
						const proyect_name 	= element.dataset.path.split( "/" ).reverse()[0]; 	



						if( type == "file" || type == "folder"  ) 
						{
							net.ajax 
							({
								url  : "server/requests.php?create_file",
								post : 
								[
									{ name : "nombre"  , value : text } ,
									{ name : "proyect" , value : proyect_name} ,
									{ name : "type"    , value : type }
								],
								response: ( val ) => 
								{
									console.log( JSON.parse( val ) );

								}

							});

						}






						const nuevo = document.createElement("div");
						      nuevo.style.fontSize   = "14px";
						      nuevo.style.marginLeft = (value_type_margin + (level_dom*20) )+"px";


						    const nuevo_inter = document.createElement("div"); 				nuevo.appendChild( nuevo_inter );
						          nuevo_inter.onclick        = ( )  => editor.manager( nuevo_inter );
						 	  nuevo_inter.oncontextmenu  = ( )  => editor.contextMenu( nuevo_inter );
							  nuevo_inter.dataset.type   = type;
							  nuevo_inter.dataset.level  = level_dom;
							  nuevo_inter.className      = "boxFile";
							  nuevo_inter.dataset.level  = level_dom;
							  nuevo_inter.dataset.id_container = id_container; 
						
						if( type == "folder" )
						{
							  nuevo_inter.dataset.count_files = "0";

							const nuevo_image            = document.createElement("img");     nuevo_inter.appendChild( nuevo_image );
							      nuevo_image.src        = "imagenes/arrow.webp";
							      nuevo_image.className  = "arrowIcon";
							      nuevo_image.style.visibility = "hidden";
						}
						
							const nuevo_image_icon      = document.createElement("img");     nuevo_inter.appendChild( nuevo_image_icon );
							      nuevo_image_icon.src  = ( type == "folder" ) ? "imagenes/icon_folder.png" : "imagenes/icon_file.png";
							      nuevo_image_icon.className = "icon";
									
							const nuevo_text = document.createTextNode( text );
							      nuevo_inter.appendChild( nuevo_text );



							const  nuevo_container      = document.createElement("div"); nuevo.appendChild( nuevo_container);
							       nuevo_container.style.display = "none";
							       nuevo_container.id   = id_container;	



						document.querySelector("#"+( element.dataset.id_container) ).appendChild( nuevo );
						element.querySelector(".arrowIcon").style.visibility = "initial";


						return true;
					}

					static uploadFile( archivo , elemento )
					{
						let archivoDom = new boxFile( {
										nombre :  archivo.name,
										type   : "folder",
										path   : elemento.dataset.path,
										file   : archivo
							         });
	
						archivoDom.upload( );
						
					}

					static prepareUploadFile ( elemento ) 
					{
						let archivos = document.querySelector("#uploadFile");
						    archivos.click();
						    archivos.onchange = ( event ) =>  
						    {
							document.querySelector("#uploaderContainer").style.bottom = "10px";
							 
							for( var i = 0; i < archivos.files.length; i++ ) 
							{
								this.uploadFile( archivos.files[i] , elemento );
							}
						    };
					}

					static contextMenu ( element ) 
					{
						let menu = [];
						
						if( element.dataset.subtype == "proyect" )
						{	
							menu.push( { text : "Cerrar proyecto" } );

							menu.push( contextMenu.SYMBOL_SEPARATOR );

							menu.push( { text : "Compartir" } );
						}


						if( element.dataset.type == "file")
							menu.push( { text : "Abrir" } );
						else if( element.dataset.type == "folder" ) 
						{
							menu.push( contextMenu.SYMBOL_SEPARATOR );
							
							menu.push( { text : "Crear Carpeta" , callBack: ( ) => editor.createComponent( 
								element ,
								"folder"
							)});

							menu.push( { text : "Crear Archivo" , callBack: ( ) => editor.createComponent( 
								element ,
								"file"
							)});

							menu.push( contextMenu.SYMBOL_SEPARATOR );

						}

						if( element.dataset.subtype != "proyect" )
						{
							menu.push( { text : "Copiar" } );
							menu.push( { text : "Cortar" } );
							menu.push( { text : "Eliminar" , callBack: ( ) => editor.deleteFile( 
								element
							)} );

							menu.push( contextMenu.SYMBOL_SEPARATOR );
							if( element.dataset.type == "folder")
							menu.push( { text : "Subir archivo" , callBack : ( ) => editor.prepareUploadFile(element)} );
						}else
						{
							menu.push( { text : "Subir archivo" , callBack : ( ) => editor.prepareUploadFile(element)} );
							menu.push( { text : "Eliminar Proyecto" } );
						}
	

						menu.push( { text : "Cambiar nombre" } );
						menu.push( { text : "Descargar" } );
						
						menu.push( contextMenu.SYMBOL_SEPARATOR );
						
						menu.push( { text : "Propiedades" , callBack: ( ) => AlertManager.openAlert( "alert_file" ) } );
						
						contextMenu.createContext( event.clientX , event.clientY , menu );
						event.preventDefault();
					}

					static manager( element ) 
					{
						if( element.dataset.type == "folder" ) 
						{
							let iconArrow = element.querySelector(".arrowIcon");
							let h = document.querySelector("#"+element.dataset.id_container);
							
							    if( h.style.display == "block" ) 
							    {
								iconArrow.style.transform = "rotate(-90deg)";
								h.style.display = "none";
							    }else
							    {
								iconArrow.style.transform = "rotate(0deg)";
							    	h.style.display = "block";
						            }
						}
					}

					static domToText( elemento , separator = "" ) 
					{
						let result = "";
						let nod    = elemento.querySelectorAll("*");

						for( var i = 0; i < nod.length; i++ ) 
						{
							result += nod[i].innerHTML+""+separator;

						}
				
							return result;
					}

					static refresh( ) 
					{
						
					let elemento = document.querySelector("#editor");
						editor.setText( elemento.innerHTML );

					}


					static getLines ( front ) 
					{
						let v = front.querySelectorAll("div").length;
						
						return ( v == 0 ) ? 1 : v;
					}

					static digits ( value , num ) 
					{
						let result = (value).toString();
						
						for( var i = result.length; i < num ; i++ ) 
						result = "0" + result;

						return result;
					}

					static setText( value ) 
					{

						let back  = document.querySelector( "#editor_back");
						let front = document.querySelector( "#editor");
						let lines = document.querySelector( "#lines");

						back.innerHTML = sintaxisEditor.decode(value);

						document.querySelector("#editor").style.top = `-${editor_back.offsetHeight}px`;	

						let num = editor.getLines( front );
						lines.innerHTML = "";
							
						for( let i = 0; i < num ; i++ ) 
						{
							lines.innerHTML += "<div class=\"linesBox\">"+editor.digits((i+1),4)+"</div>";
						}

					}

					static resize ( ) 
					{
						let component1 = document.querySelector("#right").offsetHeight;
						let component2 = document.querySelector("#console").offsetHeight;
						let component3 = document.querySelector("footer").offsetHeight+4;

						document.querySelector("#editorContainer").style.height = 
						(component1 - (component2 + component3)) + "px";

					}

				};

				window.addEventListener( "load" , (event) => 
				{

					Api.log( "Bienvenido al IDE GameBoy v1.0" );

					window.addEventListener("resize" , ( event ) => editor.resize );

					let elemento = document.querySelector("#editor");
					    editor.resize();
					
					    editor.refresh(); 
					    elemento.addEventListener( "keypress" , editor.refresh );
					    elemento.addEventListener( "keydown"  , editor.refresh );
					    elemento.addEventListener( "keyup"    , editor.refresh );

					    net.addEventListener("connection" , ( val ) => 
					    {
						   document.querySelector("#footer_status_conexion").innerHTML = val.response.text;
					    });

					    net.connection( true );


				});
