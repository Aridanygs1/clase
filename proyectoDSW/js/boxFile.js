
						class boxFile
						{
							// data.nombre data.type data.path data.file
							constructor ( data )
							{
								const esto = this;

								this.file   = data.file;
								this.path   = data.path;
								this.type   = data.type;
								this.nombre = data.nombre ;

								this.dpadre    = document.createElement("div");
								this.dpadre.className = "CboxFile";
								this.dpadre.onclick = function ( ) 
								{
									if( esto.dProgress.value == esto.dProgress.max ) 
									{
										esto.dpadre.parentElement.removeChild( esto.dpadre );	
									}
								}


								this.dColRight = document.createElement("div");
								this.dColRight.className = "CboxFile_left";
								
								this.dColLeft  = document.createElement("div");
								this.dColLeft.className = "CboxFile_right";
								
								this.dProgress = document.createElement("progress");
								this.dProgress.className = "CboxFile_progress";
								
								this.dText     = document.createElement("div");
								this.dText.className = "CboxFile_text";
									
								this.dPorcen   = document.createElement("div");
								this.dPorcen.innerHTML = "0%";

								this.dName     = document.createElement("div");

								this.dImage    = document.createElement("img");
								this.dImage.className = "CboxFile_img";	




								this.dName.innerHTML =  this.nombre;
								this.dText.appendChild( this.dName );
								this.dText.appendChild( this.dPorcen );
								
								
								this.dImage.src = "imagenes/icon_file.png"

								this.dpadre.appendChild( this.dColLeft );
								this.dColLeft.appendChild( this.dImage );
								this.dpadre.appendChild( this.dColRight );

								this.dColRight.appendChild( this.dText );
								this.dColRight.appendChild( this.dProgress );

								this.print( document.querySelector("#uploaderContainer_body") ); 	
							}

							upload( ) 
							{
									const esto = this;

									let form = new FormData();
									    form.append( "path"    , this.path );
									    form.append( "archivo" , this.file );
								
									let ajax = new XMLHttpRequest( );
									    ajax.open( "POST" , "server/requests.php?uploadfile" , true );
									    
									    ajax.upload.addEventListener("progress" , ( d ) =>  
									    {
										esto.dPorcen.innerHTML = 
									( Math.round( ( d.loaded / d.total) * 100  ) ) + "%";

										esto.setProgress( d.loaded , d.total );
									    });
								
									    ajax.onreadystatechange = function( ) 
									    {
										if( ajax.readyState == 4 ) 
										{
											try
											{
												let val = JSON.parse( ajax.responseText );
												if( val.status ) 
												{
													esto.dPorcen.innerHTML = 
												    "<span style='color:green;'>Terminado</span>";
												}else
												{

													esto.dPorcen.innerHTML = 
												    "<span style='color:red;'>"+val.text+"</span>";
												}
											}catch(e ) 
											{
												esto.dPorcen.innerHTML = 
												"<span style='color:red;'>Error:"+e+"</span>";
											}
										}
									    }

									    ajax.send ( form );

							}

							setProgress( value , max ) 
							{
								this.dProgress.value = value;
								this.dProgress.max   = max;
							}

							print ( element ) 
							{
								element.appendChild( this.dpadre);
							}

						}

