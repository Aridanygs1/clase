


	class contextMenu 
	{
		static SYMBOL_SEPARATOR = null;

		static createContext( x , y , items ) 
		{
			let c = document.querySelector("#contextMenu");
			    c.style.top =  y+"px";
			    c.style.left = x+"px";
			
			let childs = c.querySelectorAll("*");
			
			for( var i = 0; i < childs.length; i++ )
			{
				c.removeChild( childs[i] );

			}
			

			for( var i = 0; i < items.length; i++ ) 
			{
				const actual = items[i];

				if( items[i] == null ) 
				{
					let n_hr = document.createElement("div");
				            n_hr.style.backgroundColor = "#00000052";
					    n_hr.style.height = "1px";

					    c.appendChild( n_hr );

				        continue;	
				}

				let box = document.createElement("div");
				    box.innerHTML = items[i].text;

				    box.onclick = function( ) 
				    {
				    	if( actual.callBack != undefined )
				    	{
				    		actual.callBack();	
					}
					
					    c.style.display = "none";
				    }
			           
				    box.style.cursor        = "pointer";
				    box.style.paddingLeft   = "5px";
				    box.style.paddingRight  = "5px";
				    box.style.paddingBottom = "5px";
				    box.style.paddingTop    = "5px";
				    box.style.fontSize      = "15px";
				    box.style.fontFamily    = "arial";

				    c.appendChild( box );
				
			};

				c.style.display = "block";
				c.focus();

		}

	};


	window.addEventListener( "load" , ( event ) => 
	{
			let contextMenu = document.querySelector("#contextMenu");

			contextMenu.addEventListener( "blur" , ( event ) => 
			{
				contextMenu.style.display = "none";
			
			});
	
	});

	
