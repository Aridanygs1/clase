		
				const NET_NOT_INIT     = 0x00; 
				const NET_DISCONNECTED = 0x01;
				const NET_CONNECTED    = 0x02;

				const TEXT_NET_ERROR_NOT_CONNECT = "No se ha podido conectar con el controlador de escritorio."; 
				const TEXT_NET_CONNECTED         = "Se ha establecido conexión con el controlador de escritorio.";
				const TEXT_NET_DISCONNECTED      = "Se acaba de perder la conexión con el controlador de escritorio.";

				class net
				{

					static CONFIG_TRY_NET   = false; // Intentar conectarse.
					static CONFIG_TIME_LOOP = 7;     // SEGUNDOS
					static interval         = null;
					static conectado        = NET_NOT_INIT;
					static events           = [];

					// response => function ; url = url ; post = post ;
					static ajax ( values ) 
					{	
						let data = new FormData( );
						
						const TYPE_CONEXION = ( values.post == undefined ) ? "GET" : "POST";

						if( TYPE_CONEXION == "POST" ) 
						{
							for( var i = 0; i < values.post.length; i++ ) 
							{
								data.append( 
									values.post[i].name , 
									values.post[i].value 
								);
							}
						}

						const VALUE_POST    = ( TYPE_CONEXION == "POST") ? data : null; 
						
						
						let ajax = new XMLHttpRequest( );
						ajax.open( TYPE_CONEXION , values.url , true );

						let res = function(){};
								
						if( values.response != undefined || values.response != null ) 
						res = values.response;



						ajax.onreadystatechange = function( ) 
						{
							if( ajax.status == 200 ) 
							{
								if( ajax.readyState == 4 ) 
								{
									res( ajax.responseText );
								}			
							}else
							{
								res( undefined );
							}
						}


						ajax.send( VALUE_POST );

					}

					static pingConexion ( ) 
					{
						/*net.ajax({
								url  : "http://127.0.0.1:8000",
								post : 
								[
									{ name : "action" , value : "ping" }	
								],
								response: ( val ) =>
								{
									net.setConnected( !(val == undefined ) );
								}
						});*/
					}

					static loop ( )
					{
						net.pingConexion( );
					}

					static setConnected( val ) 
					{
						if( net.isConnected( ) != val  || net.conectado == NET_NOT_INIT ) 
						{
							const s = (val) ? NET_CONNECTED : NET_DISCONNECTED;

							if( !(net.conectado == NET_CONNECTED) && (s == NET_CONNECTED) ) 
								Api.log( TEXT_NET_CONNECTED , "ok" );
							else if( (net.conectado == NET_CONNECTED) &&  ( s == NET_DISCONNECTED ) )
								Api.log( TEXT_NET_DISCONNECTED , "error");

							net.conectado = s;	
							net.sendEvents( "connection" , net.createMessageAJAX( val ) ); 
						}
					}

					static isConnected( ) { return net.conectado == NET_CONNECTED; }
					static createInterval( ) 
					{
						if( net.interval != null ) 
						clearInteval( net.interval );


						net.interval = window.setInterval( function ( ) 
						{
							if( !net.isConnected() && net.CONFIG_TRY_NET ) 
							{
								net.connection( net.CONFIG_TRY_NET ); 		
							}else if( net.isConnected() )
							{
								net.loop( );
							}

						}, net.CONFIG_TIME_LOOP * 1000  );

	
					}
				

					static init ( ) 
					{
						net.createInterval( );
					}

					static sendEvents( event , value ) 
					{
						for( var i = 0; i < net.events.length; i++ ) 
						{
							if( net.events[i].event == event ) 
							{
								net.events[i].callBack( value );
							}
						}
					}

					static addEventListener( event , callBack ) 
					{
						net.events.push( {
								event: event,
								callBack , callBack 
						});

					}

					
					static createMessageAJAX ( val ) 
					{
						let r = 
						{
							colorRadio: ( !val ) ? "radioRed"         : "radioGreen",
							text      : ( !val ) ? " No hay conexión" : " Conectado"
						};

						return {
								response: 
								{
									text : `<div class="${r.colorRadio}"></div>${r.text}`
								}
		
						};
					}

					static connection( tryConnect = false) 
					{
						net.CONFIG_TRY_NET = tryConnect;
						net.pingConexion( );
					}

					static sendMessage( valueAction , callBack=null ) 
					{
						let form = new FormData( );

						form.append( "action"  , valueAction );

						let ajax = new XMLHttpRequest( );


						    ajax.upload.onprogress = function( e ) 
						    {
							console.log( e );
					            }

						    ajax.onreadystatechange = function( ) 
						    {
							if( ajax.readyState == 4  )
							{
								if( ajax.status == 0 ) 
								{
									Api.log( TEXT_NET_ERROR_NOT_CONNECT , "error" );
								}else
								{
									if( callBack != null  && ajax.responseText != undefined) 
									callBack( ajax.responseText );

									console.log( ajax.responseText ) ;
								}
							}
						    }						    

						ajax.open( "POST" , "http://127.0.0.1:8000" );
						ajax.send( form );


		
					}	

				}



	net.init( );




