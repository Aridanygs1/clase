<!DOCTYPE html>
<html lang="es">
<head>
	<style>
		body 
		{
			display:flex;
			justify-content:center;
			align-items:center;
			margin:0;
			min-height:100vh;
			background-color:gray;
			background-image: url( "imagenes/login_background.webp" );
			background-size: 100% 100%;
		}

		#container 
		{
			box-shadow: 18px 19px 17px 10px black;
			display:flex;
			width:80%;
			max-width: 800px;
			height:530px;
			background-color:white;
		}

			#container_title
			{
				text-align: center;
				font-size: 19px;
				font-weight: 600;
				margin-top: -85px;
				margin-bottom: 85px !important;
			}


			#container img
			{
				height:100%;
				width:320px;
			}

			#container > div:nth-child( 2 ) 
			{
				flex:1;
				display:flex;
				flex-direction:column;
				justify-content:center;
				align-items:center;
			}

				#container > div:nth-child( 2 ) > div 
				{
					margin-bottom:10px;
				}

				#container > div:nth-child( 2 ) > div div
				{
					margin-bottom:5px;
				}


				#container > div:nth-child( 2 ) > div input:not([type="checkbox"])
				{
					width:400px;
				}

				.messageError
				{
					padding:6px;
					color:white;
					font-family:arial;
					background-color:red;
				}
	</style>
	<script src="js/class_net.js"></script>
	<script>

			function cambiarSection( seccion ) 
			{
				let com = 
				[ 
					{ id : "login"    , dom :  "#container_section_login" } ,
					{ id : "register" , dom : "#container_section_register" }
				];

				for( var i = 0; i < com.length; i++ ) 
				{
					document.querySelector( com[i].dom ).style.display = ( com[i].id == seccion ) ? "block" : "none";
				}
			}

			class loginRegister
			{

				static login ( ) 
				{
					let com = 
					{
						user : document.querySelector("#login_user"),
						pass : document.querySelector("#login_pass"),
						sess : document.querySelector("#login_cookie")	
					};

					let val = new FormData( );
				            val.append( "usuario"  , "HOla");
					    val.append( "password" , "" );

					net.ajax({
							url  : "server/requests.php?login" ,
							post : 
							[
								{ name : "user"        , value : com.user.value },
					   			{ name : "password"    , value : com.pass.value },
				    			    	{ name : "persistente" , value : com.sess.checked }  
							], 
							response : ( val ) => 
					    		{
								val = JSON.parse( val );

								if( !val.status )
								{
									let box = document.querySelector("#message_loginError");
									    box.style.display = "block";

									    if( val.text != undefined )	
									    box.innerHTML = val.text;
								}else
								{
									document.location = ".";
								}	

							}				
					});
				}

				static register( ) 
				{
					net.ajax({
							url  : "server/requests.php?register" ,
							post : 
							[
							//	{ name : "user" 
							]						

					});


				}
			};
	</script>

</head>
<body>
	<div id="container">
			<div>
				<img src="imagenes/gameboy.jpg" />
			</div>
			<div>
				<div id="container_title" >GAMEBOY IDE</div>
				
				<div id="container_section_login">
					<div>
						<b>Conectar</b>
					</div>

					<div>
						<div>Email</div>
						<input id="login_user" />
					</div>


					<div>
						<div>Contraseña</div>
						<input type="password" id="login_pass" />
					</div>

					<div style="text-align:center;">
						<input id="login_cookie" type="checkbox" /> Mantener conectado.
					</div>

					<div style="text-align:center;">
						<a href="javascript:cambiarSection('register');">¿No tienes cuenta?</a>
					</div>

					<div style="display:none;" id="message_loginError" class="messageError">
					</div>

					<div style="text-align:center;margin-top:20px;">
						<button onclick="loginRegister.login();" >Conectar</button>
					</div>
				</div>
		
				<div style="display:none;" id="container_section_register">
					<div>
							<b>Registrar</b>
					</div>

					<div>
						<div>Nombre de usuario</div>
						<input id="register_nombre" />
					</div>
					
					<div>
						<div>Email</div>
						<input id="register_email" />
					</div>

				
					<div>
						<div>Contraseña</div>
						<input id="register_clave1" type="password" />
					</div>

					<div>
						<div>Repetir Contraseña</div>
						<input id="reigster_clave2" type="password" />
					</div>
	
					<div>
						<a href="javascript:cambiarSection('login');">Conectar</a>
					</div>

					<div style="text-align:center;">
						<button onclick="loginRegister.register();" >Registrar</button>
					</div>

						
				</div>

			</div>	
	</div>

</body>
</html>
