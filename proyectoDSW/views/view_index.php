<!DOCTYPE html>
<html lang="es">
<head>
		<title>IDEGAMEBOY</title>
		<meta charset="UTF-8" />

<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="0" />

		<style>

				.CboxFile
				{
					height: 60px; 
					display: flex; 
					border-bottom: 2px solid gray;
				}

					.CboxFile_left
					{
						flex: 1 1 0%;
						display: flex;
						justify-content: center;
						flex-direction:column;
					}

					.CboxFile_right
					{
						 width:60px;
						 display: flex;
						 flex-direction: column;
						 justify-content: center;
					}

					.CboxFile_text
					{
						padding-left:18px;
						font-family:arial;
						padding-right:10px;
						display:flex;
						justify-content:space-between;
					}

					.CboxFile_img
					{
						width: 45px;
						height: 44px;
						display:flex;
						justify-content:center;
						align-items:center;
					}

					.CboxFile_progress
					{
						height: 25px;
						border: 0;
						width: 90%;
						margin: 0 auto;
						appearance: none;
					}

					

		</style>

		<link href="css/index.css" rel="stylesheet" />
		<link href="css/gui_proyectManager.css" rel="stylesheet" />


		<script src="js/Api.js"></script>	
		<script src="js/class_net.js"></script>
		<script src="js/class_editor.js"></script>
		<script src="js/class_contextMenu.js"></script>
		<script src="js/Tools.js"></script>
		<script src="js/AlertManager.js"></script>
		<script src="js/boxFile.js"></script>
</head>
<body>
		<input type="file" style="display:none;" id="uploadFile" multiple />

		<header id="header">
				<?php include "parts/part_header_1.php" ?>
				<?php include "parts/part_header_2.php" ?>
		</header>

		<div id="container">

			<div id="left"> <?php $proyectFileManager->print( ); ?> </div>

			<div id="right">
				<div id="barTabs">
					<div class="barTabs_box">main.asm <span>x</span></div>	
					<div class="barTabs_box">graphics.asm <span>x</span></div>
				</div>
				<div id="editorContainer">
					<div id="lines"></div>
					<div style="width:100%;height:100%;">
						<div class="editorGroup" id="editor_back">Hola</div>
						<div autofocus spellcheck="false" class="editorGroup" 
						     contenteditable id="editor" 
						     autocomplete="off"></div>
					</div>
				</div>

				<div id="console">
						<div >Consola</div>
						<div id="console_container"></div>
				</div>
			</div>

		</div>
	
		<footer>
				<span id="footer_status_conexion" style="margin-left:10px;"> 
					<div class="radioOrange" ></div> Esperando conexión...
				</span>
		</footer>


		<div id="capa" style="z-index:100;display:none;" >
				<?php
				
					AlertManager::print_alert_fileInfo( );
					AlertManager::print_alert_config( ) ;	

				?>
		</div>


		<div id="uploaderContainer" 
		style="z-index:0;display:flex;flex-direction:column;position:fixed;bottom:-400px;height:200px;width:400px;right:10px;border:4px solid gray;">
			<div style="background-color:gray;height:25px;display:flex;align-items:center;color:white;justify-content:space-between">
				<div>Archivos subiendo</div>
				<div>
					<button onclick="
let a = document.querySelector('#uploaderContainer'); a.style.bottom = ( a.style.bottom == '10px' ) ? '-179px' : '10px'; "
style="appearance:none;border:0;">-</button>
	<button onclick="document.querySelector('#uploaderContainer').style.bottom = '-400px';" style="appearance:none;border:0;">x</button>	
				</div>
			</div>
			<div style="background-color:white;flex:1;overflow-y:auto;" id="uploaderContainer_body">


			</div>
		</div>

		<div id="notifyAlert" 
style="display:none;position:fixed;z-index:101;font-size:20px;align-items:center;padding:8px;bottom:10px;border:1px solid white;right:10px;background-color:black;color:white;width:320px;height:90px;">
				<div id="notifyAlert_image">
						<img src="imagenes/db.webp" style="width:80px;height:80px;margin-right:10px;" />
				</div>
				
				<div id="notifyAlert_text">
					Transmitiendo datos...
				</div>
		</div>


		<div id="contextMenu" style="z-index:10000" tabindex="-1"></div>

		<div id="loading" style="position: fixed;
top: 0px;
flex-direction:column;
left: 0px;
display: flex;
justify-content: center;
align-items: center;
z-index: 9999999;
width: 100vw;
height: 100vh;
background-color: black;
color: white;
transition:1s;
font-size: 50px;">
			<div style="margin-bottom:10px;">	Cargando ... </div>
			<progress></progress>
		</div>

	<script>
			window.addEventListener("load", function( ) 
			{
				let a = document.querySelector("#loading");
				a.style.opacity = "0";

				window.setTimeout( function( ) 
				{
					a.style.display = "none";
				} , 1500 );
			});	
	</script>
</body>
</html>

