
		<script>
					function saved( post , value , type ="text" ) 
					{
						let f = document.querySelector("#notifyAlert");
						    f.style.display = "flex";

						let form = new FormData();
						    form.append( "key"   , post );
						    form.append( "value" , value );
						    form.append( "type"  , type  );

						let ajax = new XMLHttpRequest();
						    ajax.open("POST" , "server/requests.php?saveconfig" , true );
						    ajax.onreadystatechange = function( ) 
						    {
							if( ajax.readyState == 4 ) 
							{
								window.setTimeout( function ( ) 
								{
									f.style.display = "none";
								}, 1500 );	
							}

						    }
						    
						    ajax.send( form );

					}


					class slider
					{
						static click( element , modify = false ) 
						{
							element = document.querySelector("#"+element);
							let radio = element.querySelector("div");


							if( element.dataset.value == "true" ) 
							{
								radio.style.marginLeft = "0px";
								element.style.backgroundColor = "lime";
								if( modify ) element.dataset.value = "false";
							}else
							{

								if( modify ) element.dataset.value = "true";
								radio.style.marginLeft = "30px";
								element.style.backgroundColor = "red";
							}
							
							
							if( modify ) 
							{
								saved('auto_retry_connect' ,
								(element.dataset.value == "true" ),	
								 'int'  )
							}
						}

					}
				class alertConfig_menu
				{
					static select( id , element ) 
					{
						element.style.backgroundColor = "#0000ff70";
						element.style.color ="white";

						let d = document.querySelector("#"+id);
						let l_menu = document.querySelectorAll( ".alert_config_menu" );
						let l_con  = document.querySelectorAll( ".configAlert_apart" );
					
						for( var i = 0; i < l_menu.length; i++ ) 
						{
							if( l_menu[i] != element ) 
							{
								l_menu[i].style.color = "black";
								l_menu[i].style.backgroundColor = "white";
							}
						}

						for( var i = 0; i < l_con.length; i++ ) 
						{
							l_con[i].style.display = ( l_con[i] == d ) ? "block"  : "none";
						}
					}
				}

					window.addEventListener("load", ( ) => 
					{

						document.querySelector("#sintaxis_list").value = "%input_value3";
					});
		</script>

		<style>

				.config_menu_selected
				{
					background-color: #ffab11;
					padding: 5px;
					color: white;
					font-weight: 600;
					font-size: 14px;
				}	
	
				.config_menu
				{
					padding:5px;
				}	

				#config_section_menu
				{
					    border-right: 5px solid #0775ea;
					    width: 300px;
					    height: 100vh;
					    background-color: #9999991a;
				}

				#config_section_menu_header
				{
					height: 35px;
					background-color: #0775eae6;
					display: flex;
					align-items: center;
					justify-content: center;
					color: white;
					font-weight: 600;
					font-family: arial;
				}

				.slider
				{
					width: 60px;
					border: 1px solid #8080804d;
					border-radius: 33px;
					background-color: lime;

				}

				#config_section_content
				{
					flex:1;
				}

					.slider > div 
					{
						border-radius: 100%;
						height: 30px;
						width: 30px;
						background-color: white;
					}
				
				.box_ajustes
				{
					border:2px solid blue;
					padding:6px;			
					margin-bottom:10px;	
				}

					.box_ajustes_des 
					{
						color:gray;
					}

					.box_ajustes_title
					{
						font-size:15px;
						font-weight:600;
						font-family:arial;
						margin-bottom:5px;
					}

					.box_ajustes_content
					{
						display:flex;
						justify-content:end;
					}

					.alert_config_menu
					{
						padding:4px;	
						cursor:pointer;
					}

		</style>

</head>
<body>

	<div style="display:flex;margin:-10px;">
		<div style="border-right:2px solid blue;">
			<div class="alert_config_menu" 
				style="background-color:#0000ff70;color:white;"
				onclick="alertConfig_menu.select( 'configAlert_0' , this );" >Configuración Editor</div>
			<div class="alert_config_menu" onclick="alertConfig_menu.select( 'configAlert_1' , this );">Configuración Archivos</div>
		</div>

		<div style="flex:1;padding:10px;">


			<div id="configAlert_1" style="display:none;" class="configAlert_apart">
				<div class="box_ajustes" >
					<div class="box_ajustes_title">Comunicación Controlador</div>
					<div class="box_ajustes_des">
						Si realizas conexiones a intervalos el navegador se irá comunicando con el controlador de 
						escritorio en intervalos.
					</div>	
					<div class="box_ajustes_content">
					
						<select style="width:100%;" 
							onblur="saved('auto_retry_connect' , this.value  );" 
						>
							<option value="no" selected> No realizar conexión en intervalos.</option>
							<option value="si"> Realizar conexión a intervalos</option>
						</select> 
					</div>
					
				</div>

			</div>

			<div id="configAlert_0" class="configAlert_apart">
				
				<div class="box_ajustes" >
					<div class="box_ajustes_title">Dirección del servidor escritorio</div>
					<div class="box_ajustes_des">
						Conectarse a un dispositivo que tiene instalado la app controladora de 
						escritorio.	
					</div>	
					<div class="box_ajustes_content">
					
						<input onblur="saved('server_ip' , this.value );" 
								value="%input_value0" style="margin-top:8px;width:100%;"  />
					</div>
					
				</div>

				<div class="box_ajustes">
					<div class="box_ajustes_title">Alias de host</div>
					<div class="box_ajustes_des">
						Use un alias 						
					</div>	
					<div class="box_ajustes_content">
					
						<input onchange="saved('alias' , this.value );" 
								value="%input_value1" style="margin-top:8px;width:100%;" />
					</div>
					
				</div>
				<div class="box_ajustes">
					<div class="box_ajustes_title">Descripción de host</div>
					<div class="box_ajustes_des">
						Descripción				
					</div>	
					<div class="box_ajustes_content">
					
						<textarea style="width:100%;resize:none;padding:4px;height:100px;margin-top:15px;text-align:left;" 
								onchange="saved('descripcion' , this.value );">%input_value2
						</textarea>
					</div>
					
				</div>

				<div class="box_ajustes"> 
					<div class="box_ajustes_title">Sintaxis Editor</div>
					<div class="box_ajustes_des">
						Sintaxis Editor			
					</div>	
					<div class="box_ajustes_content">
					
						<select style="width:100%;margin-top:15px;" id="sintaxis_list"  
						onchange="saved('sintaxis' , this.value );" >
								<option value="java">Java</option>
								<option value="C">C</option>
								<option value="C++">C++</option>
								<option value="Perl">Perl</option>
								<option value="PHP">PHP</option>
								<option value="ASM">ASM</option>

						</select>
					</div>
					
				</div>
			</div>
		</div>
	</div>
