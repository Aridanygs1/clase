<?php

	class AlertManager
	{
		public static function createAlert( $v_id , $a = [] , $b = []  , $width = 300 , $height = 300 , $js = "" ) 
		{
				
			array_push( $a , "%width" );
			array_push( $b , $width );
			
			array_push( $a , "%height" );
			array_push( $b , $height );

			array_push( $a , "%id" );
			array_push( $b , $v_id );
			
			array_push( $a , "%js" );
			array_push( $b , $js );
		

			$result = Util::reemplaceTemplate ( "alert.template.php" , $a , $b ) ; 
			
			$result .= "<script> AlertManager.addAlert( '$v_id' ,  '$v_id'  );</script>";

			return 	$result;	
		}






		public static function print_alert_fileInfo ( )
		{

			echo AlertManager::createAlert
			( 
					"alert_file" , 
					[ "%title" , "%content" ] , 
					[ "Alerta" , Util::readArchivo( "views/content/fileinfo.content.php" )] , 
					410 , 400
			);	

		}

		public static function print_alert_config ( ) 
		{

			$con = User::getConfig();

				echo AlertManager::createAlert( "alert_config" , 
					
					[ "%title" , "%content" , 
			       			"%input_value0" , 
						"%input_value1" ,
						"%input_value2" ,
						"%input_value3"
					] , 
					[ 
						"Configuración" , 
						Util::readArchivo( "views/content/config.content.php" ),
						$con->server_ip,
						$con->alias,
						$con->descripcion,
						$con->sintaxis
					] , 
					910 , 572);	


		}









	};





?>
