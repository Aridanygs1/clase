<?php



	class Util
	{
		public const TEMPLATE_ABECE = [ "a" , "b" , "c" , "d" , "e" , "f" , "g" , "h" , "i" , "j" , "k" , "l" , "m" , "n" , "o" ,"p" ,"q" ,"r" , "s" ,
						"t" , "u" , "v" , "w" , "x" , "y" , "z" , "0" , "1" , "2" , "3" , "4" , "5" , "6" , "7" ,"8" , "9" 
		      			      ];

		public static function readArchivo  ( $nombre , $defaultValue = "" )
		{
			$m_result  = $defaultValue;
			$PATHFILE  = getAbsolutePathProyect()."$nombre";

			$m_archivo = fopen( $PATHFILE , "r" );

			if( $m_archivo != null ) 
			{
				$m_result   = fread( $m_archivo , filesize( $PATHFILE )  );
				fclose( $m_archivo );
			}


			return $m_result;


		}

		public static function reemplaceTemplate( $nombre_template , $a = []  , $b = []  , $defaultValue = "" )
		{
			$m_result  = $defaultValue;
			$PATHFILE  = getAbsolutePathProyect()."views/template/$nombre_template";


			$m_archivo = fopen( $PATHFILE , "r" );

			if( $m_archivo != null ) 
			{
				$m_value   = fread( $m_archivo , filesize( $PATHFILE )  );
				$m_result  = str_replace( $a , $b , $m_value );
				fclose( $m_archivo );
			}


			return $m_result;
		}


		public static function createHash( $longitud , $texto = "" , $data = null ) 
		{
			$values    = $data ?? self:: TEMPLATE_ABECE;
			$longitud -= strlen( $texto );			

			for( $i = 0 ; $i < $longitud ; $i++ ) 
			{
				$texto .= $values[ rand( 0 , count( $values )-1 ) ];
			}

			return $texto;

			//97 122
			//48 57
		}

		public static function createCookie( $k , $v , $t = null )
		{
			$t = $t ?? time()+86400;

			if( PHP_VERSION_ID >= 70300 ) 
			{
				return setcookie( $k , $v , 
				[
    					'expires' => $t,
    					'path' => '/',
    					'domain' => 'domain.com',
    					'secure' => true,
    					'httponly' => true,
    					'samesite' => 'None',
				]);
			}else
			{
				return setcookie( $k , $v, $t, '/; samesite=strict');
			}
		}

		public static function createMessage( $estado , $text = ""  ) 
		{
			echo JSON_encode( array( "status" => $estado , "text" => $text ) );
		        exit;	
		}

		public static function existOnArray( $v , $list ) 
		{

			foreach( $list as $val  ) 
			{
				if( !isset($v[$val]) ) 
				return false;
			}

			return true;

		}
		

		public static function getDay( $addDay = 0 , $type = "number" )
		{
			$d = time() + 60*60*24*$addDay;
			return [ "date" => date("Y-m-d",$d ) , "number" => $d ];


		}	


	};


	const BASE_PROYECT = "clase/proyectoDSW/";

	function getAbsolutePathProyect( ) 
	{
		return $_SERVER[ "DOCUMENT_ROOT"] . "/".BASE_PROYECT;
	}

	function require_once_absolute( $path ) 
	{
		require_once( 
			getAbsolutePathProyect()."$path" 
		);
	}


?>	
