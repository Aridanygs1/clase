<?php

	class proyectManager
	{
		private $path; 
		public function __construct( $path ) 
		{
			$this->path = $path;
		}

		public static function getStruct( $path ) 
		{
			$lista = scandir( $path );
			$result = array();

			for( $i = 2; $i < count($lista); $i++ ) 
			{
				$actu = $lista[$i];
				$ab_path = $path."/".$actu;

				array_push(
					$result,
					[
						"type"=> ( is_dir( $ab_path ) ) ? "folder" : "file",
						"name"=>$actu,
						"absolute"=>$ab_path
					]
				);

			}
		
			return $result;
		}

		public function getContent( ) 
		{
			return proyectManager::getStruct( $this->path );
		}

	};


?>
