<?php

		class User 
		{
			private $conectado = false;
			private $nombre;
			private $email;

			private $id_usuarios;


			public function __construct( $nombre  = null , $email =null ,$id_usuarios = null ) 
			{
				$this->nombre        = $nombre ?? $this->nombre;
				$this->email  	     = $email  ?? $this->email;;
				$this->id_usuarios   = $id_usuarios ?? $this->id_usuarios;
			}


			public function getID( )     { return $this->id_usuarios; }
			public function getEmail( )  { return $this->email; }
			public function getNombre( ) { return $this->nombre; }



			public function isConnected( ) 
			{
				if( isset( $_SESSION["usuario"] ) ) 
				{
					return true;
				}else if( isset($_COOKIE["p_session"] )) 
				{
					return true;	
				}

				return false;
			}



			public function setConfig( $k , $v , $t ) 
			{
				SQL::query_setConfigUser( $k , $v , $t  );
			}

			public function getConfig(  ) 
			{
				return SQL::query_getConfigUser( ); 
			}



			public static function getUser( ) 
			{
				$val = unserialize( $_SESSION["usuario"] );
				if( is_array( $val ) ) 
				{
					return new User( $val["nombre"] , $val["email"] , $val["id_usuarios"] );


				}else 	return new User ( $val->nombre , $val->email , $val->id_usuarios );	
			}



			public static function loginByCookie( $data ) 
			{
			}

			// data["nombre"] data["password"] data["persistente"]
			// RETURN: true=> login éxitoso. false=> Nombre/Clave incorrecto.
			public static function login( $data ) 
			{
				$user = SQL::query_login (
				[ 
					"nombre"   	 => $data["nombre"],
					"require_cookie" => $data["persistente"], 
					"password" 	 => $data["password"] 
				]);

				
				if( $user != null ) 
					return $_SESSION["usuario"] =  serialize( $user );
				else
					return null;

			}




		};


?>
