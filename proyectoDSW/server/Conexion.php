<?php
	session_start();

	require_once( $_SERVER["DOCUMENT_ROOT"]."/clase/proyectoDSW/server/Util.php" );

	require_once_absolute( "server/Config.php");
	require_once_absolute( "server/SQL.php" );

	
	class conexion
	{

		private $database;
		private $user;
		private $password;
		private $server;
		private $status_connect = false;

		public function __construct( $data ) 
		{
			$this->database = $data["database"];
			$this->user     = $data["user"];
			$this->pasword  = $data["password"];
			$this->server   = $data["server"];

			
			try
			{
				$text_conexion =  "mysql:dbname=$this->database;host=$this->server"; 
				$this->conexion = new PDO( $text_conexion , $this->user , $this->password );

				$this->conexion->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
				$this->status_connect = true;

			}catch(PDOException $e ) 
			{
				echo $e->getMessage();
				exit;
			}


		}


		public function isConnect( ) 		
		{
			return $this->status_connect;
		}

		public function getConexion( ) : Object
		{
			return $this->conexion;
		}

	};


	$G_conexion = new conexion([
		"user"     => config::DATABASE_USER ,
		"database" => config::DATABASE_DATABASE,
		"password" => config::DATABASE_PASSWORD,
		"server"   => config::DATABASE_SERVER		
	]);



	SQL::requestLogin( );

?>
