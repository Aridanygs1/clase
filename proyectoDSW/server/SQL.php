<?php
		require_once( "Util.php"); 
		class SQL 
		{
			// ---------- RESPONSE ---------------
			public const RESPONSE_NO_DATA   = null;	// No hay datos en la consulta.
			public const RESPONSE_ERROR     = 0x00; // Error desconocido.
			public const RESPONSE_NO_PARS   = 0x01; // No hay suficientes parámetros o son incorrectos.
			public const RESPONSE_OK        = 0x02; // Todo bien.
			// -----------------------------------
			
			// Login usuario con la base de datos.
			public const QUERY_LOGIN_USER         = "SELECT * FROM usuarios WHERE nombre = :usuario AND clave = :clave";
			// Registro usuario.
			public const QUERY_REGISTER_USER      = "INSERT INTO ( nombre , clave ) VALUES ( :usuario , :clave )";
			// Borrar usuario.
			public const QUERY_DELETE_USER        = "DELETE FROM usuarios WHERE id_usuario = :id";
			// Crear cookie para un usuario.
			public const QUERY_COOKIE_SESSION     = "INSERT INTO usuarios_session ( fk_usuarios , code , caduca ) VALUES (:id_usuario , :code , :caduca)";
			// Obtiene usuario mediante la cookie.
			public const QUERY_USER_BY_COOKIE     = "SELECT * FROM usuarios u where u.id_usuarios = ( SELECT c.fk_usuarios  FROM usuarios_session c 
								 where  c.code = :cookie_code )";

			public const QUERY_USER_CONFIG        = "SELECT * FROM configuracion  WHERE kf_usuario = :id ";

			public static function query_setConfigUser( $k , $v , $type = "text" ) 
			{
				global $G_conexion;

				$f = "'%s'";

				if( $type == "int" )
				{
					$f = "%u";
					$v = intval($v);
				}

				$sentencia = sprintf( "UPDATE configuracion SET %s = $f  WHERE kf_usuario = %u" , $k , $v , User::getUser()->getID() );
				
				$consulta = $G_conexion->getConexion()->prepare( $sentencia);
				$consulta->execute();
			}



			public static function query_getConfigUser( ) 
			{
				global $G_conexion;

				$consulta = $G_conexion->getConexion()->prepare( self::QUERY_USER_CONFIG );
				$consulta->bindValue( ":id" , User::getUser()->getID( ) );
				$consulta->execute();

				return $consulta->fetch( PDO::FETCH_OBJ );
			}


			// Solicita login. En caso de tener una cookie y no usa sesión se le 
			// crea una.
			public static function requestLogin(  ) 
			{
				if( isset( $_COOKIE["p_session"] )  AND !isset($_SESSION["usuario"] ) ) 
				{
				        global $G_conexion;	
					$c = $_COOKIE["p_session"];


					$consulta = $G_conexion->getConexion()->prepare( self::QUERY_USER_BY_COOKIE );
				   	$consulta->setFetchMode( PDO::FETCH_CLASS , "User"  );
					$consulta->bindParam( ":cookie_code" , $c );
					$consulta->execute( );

						$_SESSION["usuario"] = serialize($consulta->fetch());							
				
				}
			}

			//////////////////////////////////////////////////////////////////////////
			// Realiza LOGIN en la base de datos mediante la contraseña del usuario  / 
			// y su nombre.								 /
			// ----------------------------------------------------------------------/
			// PARS:   - *(String)  data["nombre"]         => Nombre de usuario.     / 
			// 	   - *(String)  data["password"]       => Contraseña del usuario./
			// 	   -  (Boolean) data["require_cookie"] => Pedir cookie de sesión./
			//-----------------------------------------------------------------------/
			// RETURN: - const (Int) RESPONSE_NO_DATA      => Usuario no encontrado. /
			//         - const (Int) SQL::RESPONSE_NO_DATA => No hay usuario.        /
			//////////////////////////////////////////////////////////////////////////
			public static function query_login( $data ) 
			{
				global $G_conexion;

				$user_login = null;
				$data["require_cookie"] = $data["require_cookie"] ?? false;
			
				$consulta = $G_conexion->getConexion()->prepare( SQL::QUERY_LOGIN_USER );
				$consulta->bindParam( ":usuario" , $data["nombre"] );
				$consulta->bindParam( ":clave"   , $data["password"] );

				// ======== CONFIGURAR LAS CONSULTAS =========
				   $consulta->setFetchMode( PDO::FETCH_CLASS , "User"  );
				   $consulta->execute();
				   $user_login = $consulta->fetch( );
				// ===========================================

				if( $consulta->rowCount() > 0 )  		
				{	
					$_SESSION["usuario"] = serialize( $user_login );


					if( $data["require_cookie"] && $user_login != null )
					{
						$COOKIE_HASH  = Util::createHash( 30 , $user_login->getID() );
						$COOKIE_TIME  = Util::getDay( 20 );

						$consulta = $G_conexion->getConexion( )->prepare( SQL::QUERY_COOKIE_SESSION );
						$consulta->bindValue( ":id_usuario" , $user_login->getID() );
						$consulta->bindValue( ":code" ,       $COOKIE_HASH ); 
						$consulta->bindValue( ":caduca" ,     $COOKIE_TIME["date"] );

						if( $consulta->execute( ) ) 
						{
							Util::createCookie( "p_session" , $COOKIE_HASH , $COOKIE_TIME["number"] );	
						}

					}




					return $user_login;	
				}

				return SQL::RESPONSE_NO_DATA;

			}	

		}


?>
