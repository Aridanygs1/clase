<?php
	require_once( "Conexion.php" );
	require_once( "Util.php" );
	require_once( "FileManager.php");
	

	if( count( $_GET ) > 0 )
	{
		switch( key($_GET) ) 
		{
			case "uploadfile": 

				if( isset($_POST["path"]) AND isset($_FILES["archivo"])  ) 
				{
					FileManager::uploadFile( $_FILES["archivo"] , $_POST["path"] );	
				}

				Util::createMessage( "Parámetros mal enviados" );

		 	break;
			case "saveconfig":
				require_once("User.php");


				User::setConfig( $_POST["key"] , $_POST["value"] , $_POST["type"] );

			break;
			case "login":

				require_once( "User.php");

				if( Util::existOnArray( $_POST , [ "user" , "password" , "persistente" ] ) ) 
				{
					if( User::login( [ "nombre" => $_POST["user"] , 
							   "password" => $_POST["password"]  , 
							   "persistente" => $_POST["persistente"] ] )  != null) 
					{
						Util::createMessage( true );
					}

					Util::createMessage( false , "Login incorrecto.");
				}
					Util::createMessage( false , "Fallo en el envio de la petición." );
			break;
			case "delete_file":

				if( Util::existOnArray( $_GET , [ "path" , "type" ] ) ) 
				{
					fileManager::deleteFile( $_GET["path"] , $_GET["type"] );
					Util::createMessage( true );
				}

				Util::createMessage( false , "Fallo en el envío de la petición.");
			break;
			case "create_file" :
				
				$verificar = Util::existOnArray ( $_POST,
				[
					"nombre" , "proyect" , "type"
				]);				

				if( $verificar ) 
				{
					fileManager::createFile( $_POST["nombre"] , $_POST["proyect"] , $_POST["type"] );
					Util::createMessage( true );
				}

				Util::createMessage( false , "Fallo en el envío de la petición" );
	
			break;
			default: Util::createMessage( false , "La operación especificada no corresponde con ninguna existente.");


		}
		

	}else
	{
		Util::createMessage( false , "No se ha especificado la operación.");

	}


?>
