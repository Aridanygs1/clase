-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 14-12-2020 a las 12:46:02
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyectophp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE `configuracion` (
  `id_configuracion` int(11) NOT NULL,
  `kf_usuario` int(11) NOT NULL,
  `server_ip` varchar(16) DEFAULT NULL,
  `auto_retry_connect` tinyint(4) DEFAULT '1',
  `alias` varchar(25) NOT NULL,
  `descripcion` text NOT NULL,
  `sintaxis` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id_configuracion`, `kf_usuario`, `server_ip`, `auto_retry_connect`, `alias`, `descripcion`, `sintaxis`) VALUES
(1, 1, 'localhost2', 0, 'pepe', 'freofkerkgerkg', 'ASM'),
(2, 2, 'aaaaaaaaa', 1, '2131313', '						dwqÃ±dpwÃ±lfkwepafkerligj\r\n', 'C');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuarios` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `email` varchar(25) NOT NULL,
  `clave` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuarios`, `nombre`, `email`, `clave`) VALUES
(1, 'root', 'root@gmail.com', '123'),
(2, 'aridany', 'aridanyfps@gmail.com', '123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_session`
--

CREATE TABLE `usuarios_session` (
  `id_usuarios_session` int(11) NOT NULL,
  `fk_usuarios` int(11) NOT NULL,
  `code` varchar(30) NOT NULL,
  `caduca` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios_session`
--

INSERT INTO `usuarios_session` (`id_usuarios_session`, `fk_usuarios`, `code`, `caduca`) VALUES
(19, 1, '1i40uut81cp05q8i5tqtl4q2s11dgj', '2021-01-01'),
(20, 1, '1qqpmxfj1u3ypj3wiabqwva35pfvir', '2021-01-01'),
(21, 1, '1r9et5agiky28b4wtvjyzmew26zyu4', '2021-01-01'),
(22, 1, '1rbm4p5ikjhgwlme7il0oscmie2mr4', '2021-01-01'),
(23, 1, '1s6cm8wq2lhmf4jza8dw3i88m5pa1k', '2021-01-01'),
(24, 1, '1spxsvvpqt2mqie141lsxshv7f1egk', '2021-01-03'),
(25, 1, '10qivfwacii4pxtgjtfhlbygw31ime', '2021-01-03'),
(26, 1, '15p61hztaw2mnlkd6s472tb7z8ouza', '2021-01-03'),
(27, 1, '1q6j1wpoceacg9zy7o2iozqfbgir4t', '2021-01-03'),
(28, 1, '1dvpdqt9eu19554f6jgqz7wor1fhj4', '2021-01-03'),
(29, 1, '1jkdgpy0kua4ccawx8rd2vbyhdwps9', '2021-01-03'),
(30, 1, '1i5m3vjyv7jslec8l2xe32h9j8pl82', '2021-01-03'),
(31, 1, '11nihe0h1kbkyv00biydp7bz5z45zt', '2021-01-03'),
(32, 1, '10wdmx62lok2dixbtr7qfy1a937eqw', '2021-01-03'),
(33, 1, '1bcg7ngeir6ffvlsj68fc1p57ua0bx', '2021-01-03'),
(34, 2, '2njfsv5gei9idu16vzeij0e92cduhh', '2021-01-03'),
(35, 2, '2nrmgc2cnlelfw3kfs2tcepjztwneh', '2021-01-03');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD PRIMARY KEY (`id_configuracion`),
  ADD KEY `kf_usuario` (`kf_usuario`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuarios`);

--
-- Indices de la tabla `usuarios_session`
--
ALTER TABLE `usuarios_session`
  ADD PRIMARY KEY (`id_usuarios_session`),
  ADD KEY `fk_usuarios` (`fk_usuarios`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  MODIFY `id_configuracion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuarios` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuarios_session`
--
ALTER TABLE `usuarios_session`
  MODIFY `id_usuarios_session` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD CONSTRAINT `configuracion_ibfk_1` FOREIGN KEY (`kf_usuario`) REFERENCES `usuarios` (`id_usuarios`);

--
-- Filtros para la tabla `usuarios_session`
--
ALTER TABLE `usuarios_session`
  ADD CONSTRAINT `usuarios_session_ibfk_1` FOREIGN KEY (`fk_usuarios`) REFERENCES `usuarios` (`id_usuarios`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
