




	class Form_control
	{
		static method_delete ( req , res ) 
		{
			g_db.remove( "bandera" , { _id : req.query.id } , ( err ) =>  
			{
				res.redirect( ".");
			});
		}

		static method_put ( req , res ) 
		{
			g_db.update(	"bandera" , { _id : req.body.id },  
			{ 
				nombre  : req.body.nombre,
				dir     : req.body.dir,
				colores : req.body.colors
			} , (err ,val ) => 
			{
				if( err == null ) 
					res.json( { type : "ok" , text : "Se ha actualizado con éxito." } );
				else
					res.json( { type : "error" , text : "No se pudo actualizar." } );

			});
		}


		static method_get (req , res )
		{


			let action = g_Util.validateGET( req.query , {
				key     : "action",
				default : "create",
				values  : [ "action" , "edit" ]	
			} );


			let valueID =  (req.query.id == undefined ) ? "" : req.query.id; 

			let plantilla = 
			{
					id      : 0,
					nombre  : "",
					texto   : action ,
					colors  : "" , 
					dir     : ""
			};

			banderas_model.getBanderaById( valueID , ( err , data ) => 
			{

				if( err == null ) 
				{
					plantilla.id     = valueID;
					data = data[0];
					plantilla.nombre = data.nombre;
					plantilla.texto  = action;
					plantilla.colors = data.colores;
					plantilla.dir    = data.dir;
				}

				res.render( "form.pug" , plantilla );
		
			
			}); 


		}

		static method_post (req , res ) 
		{

			if( req.body.nombre != undefined && req.body.dir != undefined && req.body.colors != undefined) 
			{
				if( req.body.nombre.length < g_configData.form.minSizeBanderaName ) 
				{
					res.json( { type : "error" , text : g_configData.text.error_MinSizeBanderaName } );
				}else if( req.body.colors.split(",").length < g_configData.form.minLinesBandera ) 
				{
					res.json( { type : "error" , text : g_configData.text.errorMinSizeLinesBandera } );
				}else
				{
					g_db.create("bandera" , 
					{
						nombre :  req.body.nombre ,
						colores : req.body.colors,
						dir     : ( req.body.dir == "horizontal") ? "horizontal" : "vertical"
					} , ( err ) => 
					{
						if(  err != null ) 
						 res.json( { type : "error" , text : "Error en la base de datos al insertar el registro." } );
						else
						 res.json( { type : "ok" , text : "Creado" } );
					});
				}
			}else
			{
				res.json( { type: "error" , text : "Faltan parámetros" } );
			}
		}

	}



	module.exports = Form_control;
