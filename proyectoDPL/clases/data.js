


	global.g_configData = 
	{
		text     : 
		{
			error_MinSizeBanderaName  : "El nombre de la bandera debe de ser 4",
			errorMinSizeLinesBandera  : "La bandera debe de tener 2 líneas como mínimo."

		},
		form     :
		{
			minSizeBanderaName : 4 ,
			minLinesBandera    : 2

		},
		server   : 
		{
			port : 8500

		},
		database : 
		{
				
				user     : "root" ,
				password : "",
				database : "dpl",
				server   : "127.0.0.1",

				esquemas : 
				[
						{
							nombre   : "bandera" ,
							struct   : 
							{
									colores : String  , 
									nombre  : String  ,
									dir     : String
							}
						},
				]

		}

	};
