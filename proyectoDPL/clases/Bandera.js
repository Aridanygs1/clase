

	class Bandera 
	{
		constructor( data = {} ) 
		{
			if( data.type   == undefined ) data.type   = "horizontal";
			if( data.colors == undefined ) data.colors = [ ];
			if( data.nombre == undefined ) data.nombre = "Sin definir";


			this.type   = data.type;
			this.colors = data.colors;
			this.nombre = data.nombre;
			this.id     = data.id;
		}


		borrar( ) 
		{
			console.log( "No se borra" );
		}

		toJSON( ) 
		{
			return { type : this.type , colors: this.colors , nombre : this.nombre , id : this.id };
		}
	}


	global.g_Bandera= Bandera;
