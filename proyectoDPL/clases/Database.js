


	class Database
	{
		constructor( data ) 
		{
			this.mongoose = require( "mongoose" );
			this.conexion = null;
			this.models   = [];

			this.password = data.password;
			this.db       = data.database;
			this.user     = data.user;
			this.server   = data.server;

			this.esquemas = data.esquemas;

			this.mongoose.connect(
				`mongodb://${this.server}:27017/${this.db}` ,  
				{ useNewUrlParser: true ,  useUnifiedTopology: true } 
			);

			this.conexion = this.mongoose.connection;
		             this.conexion.on( "error" , ( ) => console.error("Error en la conexión") );
			     this.conexion.on( "open"  , ( ) => console.log("Conectado.") );		     

			for( var i = 0; i < this.esquemas.length; i++ ) 
			{
				let actu = this.esquemas[i];
				this.models.push({
					name  : actu.nombre , 
					model : this.mongoose.model( actu.nombre , new this.mongoose.Schema( actu.struct ) )
				});
			}				
		}

		update( modelo , condi , values , callback = "" )
		{
			let m = this.getModelById( modelo );
			if( m == null ) 
			{
				console.error( "No existe el modelo "+modelo+".");
			}else
			{
				m.updateOne( condi , values ,callback );
			}

		

		}

		remove ( modelo , condi , callback = "" ) 
		{
			let m = this.getModelById( modelo );
			if( m == null ) 
			{
				console.error( "No existe el modelo "+modelo+".");
			}else
			{
				m.remove( condi , callback );
			}
		}


		getModelById( nombre ) 
		{
			for( var i = 0; i < this.models.length; i++ ) 
			{
				let actu = this.models[i];
				if( actu.name == nombre ) return actu.model;
			}

			return null;
		}

		create( model , query , callback = "" )
		{
			let m = this.getModelById( model );
			if( m == null ) 
			{
				console.error( "No existe el modelo "+model+".");
			}else
			{
				m.create( query , callback );
			}
		
		}

		find( model , query , callback = "" ) 
		{
			let m = this.getModelById( model )
		
			if( m == null ) 
			{
				console.error("No existe el modelo "+model+".");
			}else
			{
				m.find( query , callback ); 
			}

		}
		


		toString( ) 
		{
			return `Información db: DB=${this.db} ; USER=${this.user} ; SEVER=${this.server} ; PASSWORD=${g_Util.repeatChar('*',this.password.length)}`;
		}


	}










	global.g_Database = Database;
