
				class EditorBandera
				{
					static COLORES = 
					[ 
						"red" , "yellow" , "black" , "gray" , "purple" , "orange" , 
						"blue" ,"blueviolet" , "gold" , "magenta" , "lime"  , "fuchsia" , 
						"tan" , "white"  , "green" , "violet" , "blueviolet" , "lightblue"
					];

					static direction = "horizontal";

					static colorValue = EditorBandera.COLORES[0];


					static setDirection( val ) 
					{
						if( val == "horizontal" || val == "vertical" )
						{
							EditorBandera.direction = val;
							EditorBandera.repaint();
						}
					}

					static saved ( )
					{
						let com = 
						{
							nombre : document.querySelector("#banderaNombre"),
							dir    : document.querySelector("#banderaDir"),
							colors : EditorBandera.getColors().toString()
						}

						let metodo = "POST";

						let data = `nombre=${com.nombre.value}&dir=${com.dir.value}&colors=${com.colors}`;
						let actionAJAX = "form";
						 
						if( action == "edit" ) 
						{
							actionAJAX = "update";
							data += "&id="+e_id;
							console.log( data );
							metodo = "PUT";	
						}

						let ajax = new XMLHttpRequest();
						    ajax.open( metodo , actionAJAX );
						    ajax.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
						    ajax.onreadystatechange = function( ) 
						    {
							if( ajax.readyState == 4 ) 
							{		
								try
								{
									let val = JSON.parse( ajax.responseText );
									
									alert( val.text );

									if( val.type == "ok" )
									document.location=".";

								}catch( e ) 
								{
									alert("Error:"+e);
								}

							}
						    }

						    ajax.send( data );
					}

					static repaint( ) 
					{
						let elemento = document.querySelector("#lienzo");

						elemento.width  = 300;
						elemento.height = 300;

						let ctx    = elemento.getContext("2d");
						let c      = EditorBandera.getColors();
						let size   = ( ( EditorBandera.direction == "horizontal") ? elemento.width : elemento.height ) / c.length;

						for( var i = 0; i < c.length; i++ ) 
						{
							ctx.fillStyle = c[i];
							if( EditorBandera.direction == "horizontal" ) 
								ctx.fillRect( (i*size) , 0 , size , elemento.height  );
							else 
								ctx.fillRect( 0 , (i*size) , elemento.width , size );
						}
					}

					static popColor( ) 
					{
						let len = document.querySelectorAll( ".linearEditor" ).length;
						if( len > 0 ) 
						{
							let com = document.querySelectorAll( ".linearEditor" )[len-1];
							com.parentElement.removeChild( com );

							EditorBandera.repaint();
						}
					}

					static select( id ) 
					{
						EditorBandera.colorValue = EditorBandera.COLORES[id];	
					}

					static getColors( ) 
					{
						let result = [];
						let com = document.querySelectorAll( ".linearEditor" );

						for( var i = 0; i < com.length; i++ ) 
						{
							result.push( com[i].style.backgroundColor );
						}

						return result;
					}

					static addLineColor( va = null ) 
					{
						let valor = ( va == null ) ? EditorBandera.colorValue : va;
						
						let c = document.createElement("div");
						    c.className = "linearEditor";
						    c.style.backgroundColor = valor;


						let x = document.querySelector("#list_editor_body");
							
						x.appendChild( c );
						x.scrollTop = x.clientHeight;

						EditorBandera.repaint( );
					}

					static clickColor( id ) 
					{
								let a = document.querySelectorAll(".boxColor");
								for( var i = 0; i < a.length; i++ ) 
								{
									if( a[i].dataset.id == id ) 
										a[i].style.border = "4px solid black";
									else
										a[i].style.border = "4px solid transparent";
								}
								 
								EditorBandera.select( id );	
					}

					static init( ) 
					{
						EditorBandera.direction = document.querySelector("#banderaDir").value;

						for( var i = 0; i < EditorBandera.COLORES.length; i++ )
						{
							const id = i;

							let e = document.createElement("div");
						    	    e.className = "boxColor";
						    	    e.style.backgroundColor = EditorBandera.COLORES[i];
							    e.dataset.id = id;
							    e.onclick = ( ) => EditorBandera.clickColor ( id  );	    
						            e.style.border = (i == 0) ? "4px solid black" : "4px solid transparent";
							    

						            document.querySelector("#containerBox").appendChild( e );
						}



						if( action == "edit" ) 
						{
							for( var i = 0; i < e_color.length; i++ ) 
							{
								document.querySelector("#banderaNombre").value = e_nombre; 
								EditorBandera.addLineColor( e_color[i].replace("&quot;" , "" ) );
							}

							let ba = document.querySelector("#button_delete");
							    ba.style.display = "inline-block";

							    ba.href = "delete?id="+e_id;

							this.setDirection( e_dir );
						}
					}

				};

			
				window.addEventListener( "load" , ( ) => EditorBandera.init() );
