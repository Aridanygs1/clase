


	class Lienzo
	{
		static paintBandera ( elemento ) 
		{
			elemento.width  = 150;
			elemento.height = 120;

			let ctx    = elemento.getContext("2d");
			let c      = elemento.dataset.colors.split(",");
			let size   = ( (elemento.dataset.type == "horizontal") ? elemento.width : elemento.height ) / c.length;

			for( var i = 0; i < c.length; i++ ) 
			{
				ctx.fillStyle = c[i];
				if( elemento.dataset.type == "horizontal" ) 
					ctx.fillRect( (i*size) , 0 , size , elemento.height  );
				else 
					ctx.fillRect( 0 , (i*size) , elemento.width , size );
			}
		}

	};





	window.addEventListener( "load" , ( ) => 
	{
		let val = document.querySelectorAll("canvas");

		for( var i = 0; i < val.length; i++ ) 
		{
			Lienzo.paintBandera( val[i] );	

		}


	});
