
	const express      = require("express");
	const app          = express( );
	const pug          = require('pug');
	const path         = require('path');
	const body_parser  = require('body-parser');
	
	app.use(express.static(path.join(__dirname, 'public')));
	app.use(body_parser.urlencoded({extended:true}));

	// ============= VARIABLES ==================
	global.g_banderasList = [];	
	
	// ============= CLASES =====================
	require("./clases/Bandera" );
        require( "./clases/Database" );
	require("./clases/Util");
	require("./clases/data");
	
	// ==========================================
	global.g_db                  = new g_Database( g_configData.database );
	
	// =============   MODELOS  =================
	global.banderas_model        = require( "./models/banderas.model" );
	
	// ============= CONTROLLER =================
	global.index_control         = require( "./control/index.control" );
	global.form_control          = require( "./control/form.control" );
	global.error_control         = require( "./control/error.control" );
	global.ApiData               = require( "./clases/ApiData");

	app.get(    "/"                  , index_control.method_get   ); // Entrega la vista al inicio.
	app.get(    "/form"              , form_control.method_get    ); // Entre la vista del formulario.
	app.post(   "/form"              , form_control.method_post   ); // POST=formulario para guardar bandera.
	app.delete( "/delete"            , form_control.method_delete ); // DELETE=formulario para eliminar banderas.
	app.put(    "/update"            , form_control.method_put    ); // PUT=formulario para editar banderas
	app.use(    "/api/banderas"      , ApiData.showInfo           ); // Muestra API de las banderas.      	
	app.use(    "*"                  , error_control.get_404      ); // Si el método y/o recurso no es correcto se muesta un 404 error.


	

	app.listen( g_configData.server.port , 
		( ) => console.log( "Corriendo por el puerto " + g_configData.server.port ) );
