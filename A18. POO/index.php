<?php
	// Incluimos el controlador en el archivo index.php
	require_once( "mvc/controllers/index.controller.php" );
	require_once( "mvc/models/index.model.php");
	
	index_model::loadAll();				// = index_model::loadCategories()  + index_model::loadArticleDB()

	require_once( "mvc/views/index.view.php");	
?>
