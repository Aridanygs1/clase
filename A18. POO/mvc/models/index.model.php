<?php

		class index_model
		{
			private static $template = 
			[
				[ "1.jpg" , "Loka" ],
				[ "2.jpg" , "Nacimiento" ],
				[ "3.jpg" , "2 pájaros" ],
				[ "4.jpg" , "Cameio" ],
				[ "5.jpg" , "Suicidio" ],
				[ "6.jpg" , "Chicas" ],
				[ "7.jpg" , "Navidad" ],
				[ "8.jpg" , "Otoño" ],
				[ "9.jpg" , "Halloween" ],
				[ "10.jpg" , "Flor rosa" ],
				[ "11.jpg" , "Perro" ],
				[ "12.jpg" , "¡FRÍO!" ]
			];

			public static function loadAll()
			{
				self::loadCategories();
				self::loadArticlesDB();
			}

			public static function loadCategories() 
			{
				
				articleBox::addCategoria([ "Agustin" , "Luisito" , "Aridany" , "Vacío" ]);
			}

			public static function loadArticlesDB( ) 
			{

				shuffle( self::$template );
		
				for( $i = 0; $i < 3 ; $i++ )
				{
					$cat = articleBox::getCategorias()[$i];
					for( $u = 0; $u < 12; $u++)
					{
						new articleBox(
						[
							"text"      =>  self::$template[$u][1],
							"categoria" =>  $cat ,
							"views"     =>  rand(0 , 999999),
							"downloads" =>  rand(0 , 999999),
							"likes"     =>  rand(0 , 9999999) ,
							"image"	    =>  self::$template[$u][0]
						]);	
					}
				}	


			}



		};

?>
