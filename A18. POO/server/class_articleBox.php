<?php

	require_once( "class_util.php" );
	require_once( "class_component.php" );

	class articleBox extends component
	{
			private static $_id            = 1;
			private static $indexImage     = 0;
			private static $lista          = array();
			private static $categorias     = array();

			private $id        = 0;
			private $views     = 0;
			private $likes     = 0;
			private $downloads = 0;
			private $buffer    = null;
			private $image     = null;
			private $categoria = 0;
			private $texto     = "";


			const COMPONENT_ARTICLE_PATH = "web/components/article.html";


			public function getCategoria( ) { return $this->categoria; }
			
			// ARRAY data=  views , likes , downloads , image
			public function __construct( $data = null )
			{

				parent::__construct( self::COMPONENT_ARTICLE_PATH );

				$this->id          = self::$_id++;

				$this->texto       = $data["text"]        ?? "";
				$this->categoria   = $data["categoria"]   ?? null;
				$this->views       = $data["views"]       ?? 0;
				$this->likes 	   = $data["likes"]       ?? 0;
				$this->downloads   = $data["downloads"]   ?? 0;
				$this->image       = $data["image"]       ?? "1.jpg"; 

				$this->setVars( 
				[
					[
					       "%text",  
					       "%views" ,     
					       "%likes" ,     
					       "%downloads" ,    
					       "%img_portfolio" , 
					       "%id" ,
				       	       "%img_gallery" 
				       ], 
				       [
				       	       $this->texto , 
					       $this->views , 
					       $this->likes , 
					       $this->downloads , 
					       util::getImagePortFolio($this->image) , 
					       $this->id,
			       		       util::getImageGallery( $this->image ) 
				      ]
				]);


				array_push( self::$lista , $this );

			}


			public static function addCategoria( $cat ) 
			{
				if( is_array($cat) )
				{
					foreach( $cat as $l )
					{
						array_push( self::$categorias , $l );
					}
				}else
				{
					array_push( self::$categorias , $cat );
				}
			}

			public static function printCategorias()
			{
				$countIndex = 0;	
				foreach( self::$categorias as $id )
				{
					$activo = ( $countIndex > 0 ) ? "" : "active";
					echo "
						<td> 
							<a 
								onclick=\"paginador.getPaginador('$id' ,
								document.querySelector('#category1') );\"

								class=\"link $activo\" href=\"#$id\" 
						   		data-toggle=\"tab\">$id
							</a>
						</td>
					";
					
					$countIndex++;
				}

			}

			public static function getFirstCategory( )
			{
				if( count( self::$categorias ) == 0 )
				return null;
				else
				return self::$categorias[0];
			}

			public static function getLastCategory( )
			{
				if( count( self::$lista ) == 0 )
				return null;
				else
				return self::$lista[ count(self::$lista)-1];
			}



			public static function getSizeCategory( $cat ) 
			{
				$tamanio = 0;
 				foreach( self::$lista as $actu )
				{
					if( $cat == $actu->getCategoria() )
					$tamanio++;
				}
					return $tamanio;
			}	

			public static function printArticlesByCat( $cat ) 
			{
				if(  self::getSizeCategory( $cat ) == 0 )
				{
					echo "No hay contenido";
				}else
				{
					foreach( self::$lista as $actu )
					{
						if( $cat == $actu->getCategoria() )
						{
							$actu->printHTML();
						}
					}
				}

			}

			public static function getCategorias( ) 
			{
				return self::$categorias;
			}
	};





?>
