<?php


		class util
		{
			private const PATH_IMAGE_PORTFOLIO  = "images/index/portfolio/";
			private const PATH_IMAGE_GALLERY    = "images/index/gallery/";

			public static function getImagePortfolio( $data )
			{
				return self::PATH_IMAGE_PORTFOLIO."$data";

			}

			public static function getImageGallery( $data ) 
			{
				return self::PATH_IMAGE_GALLERY."$data";
			}

			// Obtener URL ABSOLUTA y permite concatenarlo con un archivo sin depender de la ruta
			// en que se encuentre el fichero.
			public static function getURI( $data = ""  ) : string
			{
				$result = "";

				if( isset($_SERVER["HTTPS"]) )
					$result .= "https://";
				else
					$result .= "http://";


				$result .= $_SERVER[ "HTTP_HOST"]."/$data";


				return $result;
			}

		};

?>
