<?php

		require_once("class_util.php");

		// Componente.
		class component
		{
			// Componente: (HTML) .
			protected $component;
			// Variables de reemplazo.
			protected $reemplazo;


			public function __construct( $urlfile = null )
			{
				if( $urlfile != null ) 
				$this->loadComponent( $urlfile );
			}

			// Cambiar/Crear variables de reemplazo.
			public function setVars( $values )
			{
				$this->reemplazo = $values;
			}

			// Imprimir el componente.
			public function printHTML()
			{
				echo str_replace( $this->reemplazo[0] , $this->reemplazo[1] ,  $this->component );

			}

			// Cargar el ccomponente.
			public function loadComponent( $archivo )
			{

				$archivo = $this->COMPONENT_ARTICLE_PATH = util::getURI( $archivo );
				$header_file = get_headers( $archivo , 1 );


				$archivo = fopen( $archivo , "r" );
				$this->component = fread( $archivo , $header_file['Content-Length'] );
				fclose( $archivo );
			}

		}

?>
